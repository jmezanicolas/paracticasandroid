package com.example.jmeza.seccion_02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {
    private ListView listView;
    private List<String> name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        listView = (ListView) findViewById(R.id.listView);
        name = new ArrayList<>();
        name.add("Gustavo");
        name.add("Jose");
        name.add("Francisco");
        name.add("Fernando");
        name.add("Pedro");
        //La forma en que mostraremos los datos
        // ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, name);
        //Establecemos el adaptador con nuestro list view
        // listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListActivity.this, "Clicked: " + name.get(position), Toast.LENGTH_LONG).show();

            }
        });
        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_item, name);
        listView.setAdapter(myAdapter);
    }

}

