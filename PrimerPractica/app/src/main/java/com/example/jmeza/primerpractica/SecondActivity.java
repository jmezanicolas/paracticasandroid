package com.example.jmeza.primerpractica;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
    private TextView txtViewAge;
    private Button btnNext;
    private RadioButton rdBtnGreeter;
    private RadioButton rdBtnFarewell;
    private SeekBar sbAge;


    private String name = "";
    private int age = 18;
    private final int MAX_AGE = 60;
    private final int MIN_AGE = 16;
    //para compartir
    public static final int GREETER_OPTION = 1;
    public static final int FAREWELL_OPTION = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            name = bundle.getString("name");
            Toast.makeText(SecondActivity.this, name, Toast.LENGTH_LONG).show();
        }

        sbAge = (SeekBar) findViewById(R.id.sbAge);
        txtViewAge = (TextView) findViewById(R.id.txtViewAge);
        btnNext = (Button) findViewById(R.id.btnNextSecond);
        rdBtnGreeter = (RadioButton) findViewById(R.id.rdButton1);
        rdBtnFarewell = (RadioButton) findViewById(R.id.rdButton2);

        sbAge.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                age = progress;
                txtViewAge.setText(age + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //Aun que no sobreescribimos con alguna funcionalidad, OnSeekBarChangeListener es una interfaz
                //y como interfaz que es, necesitamos sobreescribir todos sus metodos, aunque lo dejamos vacio
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                age = seekBar.getProgress();
                txtViewAge.setText(age + "");
                if (age > MAX_AGE) {
                    btnNext.setVisibility(View.INVISIBLE);
                    Toast.makeText(SecondActivity.this, "The max age allowed is: " + MAX_AGE + " years old", Toast.LENGTH_LONG).show();
                } else if (age < MIN_AGE) {
                    btnNext.setVisibility(View.INVISIBLE);
                    Toast.makeText(SecondActivity.this, "The min age allowed is: " + MIN_AGE + " years old", Toast.LENGTH_LONG).show();
                } else {
                    btnNext.setVisibility(View.VISIBLE);
                }
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("age", age);
                //Si el boton de geeter esta activo, opcion valdra 1 y si no valdra 2
                int option = (rdBtnGreeter.isChecked()) ? GREETER_OPTION : FAREWELL_OPTION;
                intent.putExtra("option", option);
                startActivity(intent);
                Toast.makeText(SecondActivity.this, sbAge.getProgress() + "", Toast.LENGTH_LONG).show();
            }
        });
    }
}
