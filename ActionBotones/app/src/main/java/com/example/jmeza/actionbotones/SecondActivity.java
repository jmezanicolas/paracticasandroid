package com.example.jmeza.actionbotones;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
    private TextView txtView;
    private Button btnNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        // activar flecha ir atras
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtView = (TextView) findViewById(R.id.textViewMain);
        btnNext = (Button) findViewById(R.id.buttonGo);
        String greeter = "";
        //take data for intent
        Bundle bundle = getIntent().getExtras();
        if (!bundle.isEmpty()) {
            greeter = bundle.getString("greeter");
            txtView.setText(greeter);
            Toast.makeText(SecondActivity.this, greeter, Toast.LENGTH_LONG).show();
        } else {
            greeter = "It is empty!";
            Toast.makeText(SecondActivity.this, greeter, Toast.LENGTH_LONG).show();
        }
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intet = new Intent(SecondActivity.this, ThirdActivity.class);
                startActivity(intet);
            }
        });
    }
}
