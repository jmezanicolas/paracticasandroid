package com.example.jmeza.actionbotones;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {
    private EditText editTextPhone;
    private EditText editTextWeb;
    private ImageButton imgButtonPhone;
    private ImageButton imgButtonWeb;
    private ImageButton imgButtonCamera;
    private final int PHONE_CALL_CODE = 100;
    private final int PICTURE_FROM_CAMERA = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        // activar flecha ir atras
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        editTextWeb = (EditText) findViewById(R.id.editTextWeb);
        imgButtonPhone = (ImageButton) findViewById(R.id.imageButtonPhone);
        imgButtonWeb = (ImageButton) findViewById(R.id.imageButtonWeb);
        imgButtonCamera = (ImageButton) findViewById(R.id.imageButtonCamera);
        //inicio del boton para el telefono
        imgButtonPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = editTextPhone.getText().toString();
                if (!phoneNumber.isEmpty() && phoneNumber != null) {
                    //comprovar la version actual del android que esta corriendo
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkPermission(Manifest.permission.CALL_PHONE)) {
                            Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + phoneNumber));
                            if (ActivityCompat.checkSelfPermission(ThirdActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            startActivity(i);
                        } else {
                            //Ha denegado o es la primera vez.
                            if (!shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                            } else {
                                Toast.makeText(ThirdActivity.this, "Please, enable the request permission", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                i.addCategory(Intent.CATEGORY_DEFAULT);
                                i.setData(Uri.parse("package:" + getPackageName()));
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                startActivity(i);
                            }
                        }
                    } else {
                        OlderVersions(phoneNumber);
                    }
                } else {
                    Toast.makeText(ThirdActivity.this, "Insert a phone number", Toast.LENGTH_LONG).show();
                }
            }

            private void OlderVersions(String phoneNumber) {
                Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + phoneNumber));
                if (checkPermission(Manifest.permission.CALL_PHONE)) {
                    startActivity(intentCall);
                } else {
                    Toast.makeText(ThirdActivity.this, "You decline the access", Toast.LENGTH_LONG).show();
                }
            }
        });
        //inicio del boton para el explorador web
        imgButtonWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = editTextWeb.getText().toString();
                try {
                    if (!url.isEmpty() && url != null) {
                        //Intent intentWeb = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url));
                        Intent intentWeb = new Intent();
                        intentWeb.setAction(Intent.ACTION_VIEW);
                        intentWeb.setData(Uri.parse("http://" + url));

                        //Contectas
                        Intent intentContacts = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people"));

                        //mail rapido
                        String mail = "link.edipro@gmail.com";
                        Intent intentMailTo = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + mail));
                        //mail completo
                        Intent intentMail = new Intent(Intent.ACTION_SEND, Uri.parse(mail));
                        intentMail.setType("message/rfc822");
                        intentMail.putExtra(Intent.EXTRA_SUBJECT, "Mail's title");
                        intentMail.putExtra(Intent.EXTRA_TEXT, "Hi there, I love myForm app, but...");
                        intentMail.putExtra(Intent.EXTRA_EMAIL, new String[]{"fernando@gmail.com", "rigo@gmail.com"});
                        startActivity(Intent.createChooser(intentMail, "Elige cliente de correo"));
                        //startActivity(intentWeb);
                        //startActivity(intentContacts);
                        //startActivity(intentMailTo);
                        //Telefono 2, sin permisos requeridos
                        //Intent intentPhone = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:123456789"));
                        //startActivity(intentMail);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        });
        imgButtonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCamera = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intentCamera, PICTURE_FROM_CAMERA);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICTURE_FROM_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.toUri(0);
                    Toast.makeText(this, "Resultadote" + result, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "There was an error whit the picture, ry again.", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)) {
                    //Comprobar si ha sido aceptada o denegada la peticion
                    if (result == PackageManager.PERMISSION_GRANTED) {
                        //Concedio su permiso
                        String phoneNumber = editTextPhone.getText().toString();
                        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + phoneNumber));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        startActivity(intentCall);
                    } else {
                        //No concedio su permiso
                        Toast.makeText(ThirdActivity.this, "You decline the access ", Toast.LENGTH_LONG).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private boolean checkPermission(String permission) {
        int result = this.checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}
