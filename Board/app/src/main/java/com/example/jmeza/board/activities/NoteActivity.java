package com.example.jmeza.board.activities;

import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.jmeza.board.R;
import com.example.jmeza.board.adapters.NoteAdapter;
import com.example.jmeza.board.models.Board;
import com.example.jmeza.board.models.Note;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;

public class NoteActivity extends AppCompatActivity implements View.OnClickListener, RealmChangeListener<Board> {
    private ListView listView;
    private FloatingActionButton fab;

    private NoteAdapter noteAdapter;
    private RealmList<Note> notes;
    private Realm realm;

    private int boardId;
    private Board board;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        realm = Realm.getDefaultInstance();

        if (getIntent().getExtras() != null) {
            boardId = getIntent().getExtras().getInt("id");
        }
        board = realm.where(Board.class).equalTo("id", boardId).findFirst();
        board.addChangeListener(this);
        notes = board.getNotes();
        this.setTitle(board.getTitle());
        fab = (FloatingActionButton) findViewById(R.id.fabAddNote);
        listView = (ListView) findViewById(R.id.listViewNote);
        noteAdapter = new NoteAdapter(this, notes, R.layout.list_view_note_item);
        listView.setAdapter(noteAdapter);
        fab.setOnClickListener(this);
        registerForContextMenu(listView);
    }

    private void showAlertForCreatingNote(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (title != null && title.isEmpty()) {
            builder.setTitle(title);
        }
        if (message != null && message.isEmpty()) {
            builder.setMessage(message);
        }
        View viewInflater = LayoutInflater.from(this).inflate(R.layout.dialog_create_note, null);
        builder.setView(viewInflater);
        final EditText input = (EditText) viewInflater.findViewById(R.id.editTextNote);
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String note = input.getText().toString().trim();
                if (note.length() > 0) {
                    createNewNote(note);
                } else {
                    Toast.makeText(getApplicationContext(), "The note can't be empty", Toast.LENGTH_LONG).show();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*CRUD*/
    private void createNewNote(String note) {
        realm.beginTransaction();
        Note _note = new Note(note);
        realm.copyToRealm(_note);
        board.getNotes().add(_note);
        realm.commitTransaction();
    }

    private void editNote(String noteDescription, Note note) {
        realm.beginTransaction();
        note.setDescription(noteDescription);
        realm.copyToRealmOrUpdate(note);
        realm.commitTransaction();
    }

    private void deleteNote(Note note) {
        realm.beginTransaction();
        note.deleteFromRealm();
        realm.commitTransaction();
    }

    private void deleteAll() {
        realm.beginTransaction();
        board.getNotes().deleteAllFromRealm();
        realm.commitTransaction();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_all_note:
                deleteAll();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.context_menu_note_activity, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete_note:
                deleteNote(notes.get(info.position));
                return true;
            case R.id.edit_note:
                showAlertForEditingNote("Edit Note", "Change the name of the note", notes.get(info.position));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void showAlertForEditingNote(String noteDescription, String message, final Note note) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (noteDescription != null && noteDescription.isEmpty()) {
            builder.setTitle(noteDescription);
        }
        if (message != null && message.isEmpty()) {
            builder.setMessage(message);
        }

        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_create_note, null);
        builder.setView(viewInflated);

        final EditText input = (EditText) viewInflated.findViewById(R.id.editTextNote);
        input.setText(note.getDescription());

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String noteDescription = input.getText().toString().trim();
                if (noteDescription.length() == 0) {
                    Toast.makeText(getApplicationContext(), "The text for the note is required to be edited", Toast.LENGTH_LONG).show();
                } else if (noteDescription.equals(note.getDescription())) {
                    Toast.makeText(getApplicationContext(), "The note is the same that it was before", Toast.LENGTH_LONG).show();
                } else {
                    editNote(noteDescription, note);
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        showAlertForCreatingNote("Add New Note", "Type a note for " + board.getTitle() + ".");
    }

    @Override
    public void onChange(Board element) {
        noteAdapter.notifyDataSetChanged();
    }
}
