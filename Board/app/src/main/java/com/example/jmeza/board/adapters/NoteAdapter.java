package com.example.jmeza.board.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jmeza.board.R;
import com.example.jmeza.board.models.Note;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by jmeza on 02/03/2017.
 */

public class NoteAdapter extends BaseAdapter {
    private Context context;
    List<Note> notes;
    private int layout;

    public NoteAdapter(Context context, List<Note> notes, int layout) {
        this.context = context;
        this.notes = notes;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return notes.size();
    }

    @Override
    public Note getItem(int position) {
        return notes.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(layout, null);
            vh = new ViewHolder();
            vh.description = (TextView) convertView.findViewById(R.id.txtViewNoteDesc);
            vh.createdAt = (TextView) convertView.findViewById(R.id.txtViewNoteCreateAt);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        Note note = notes.get(position);
        vh.description.setText(note.getDescription());

        DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        String createAt = df.format(note.getCreateAt());

        vh.createdAt.setText(createAt);
        return convertView;
    }

    public class ViewHolder {
        TextView description;
        TextView createdAt;

    }
}
