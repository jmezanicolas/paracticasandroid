package com.example.jmeza.board.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jmeza.board.R;
import com.example.jmeza.board.models.Board;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import static android.R.id.list;

/**
 * Created by jmeza on 02/03/2017.
 */

public class BoardAdapter extends BaseAdapter {
    private Context context;
    private List<Board> boards;
    private int layout;

    public BoardAdapter(Context context, List<Board> boards, int layout) {
        this.context = context;
        this.boards = boards;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return boards.size();
    }

    @Override
    public Board getItem(int position) {
        return boards.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(layout, null);
            vh = new ViewHolder();
            vh.title = (TextView) convertView.findViewById(R.id.textViewTitle);
            vh.notes = (TextView) convertView.findViewById(R.id.textViewNote);
            vh.createdAT = (TextView) convertView.findViewById(R.id.textViewDate);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        Board board = boards.get(position);
        vh.title.setText(board.getTitle());

        int numberOfNotes = board.getNotes().size();
        String textForNotes = numberOfNotes == 1 ? numberOfNotes + " Note" : numberOfNotes + " Notes";

        vh.notes.setText(textForNotes);

        DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        String createAt = df.format(board.getCreateAt());
        vh.createdAT.setText(createAt);
        return convertView;
    }

    public class ViewHolder {
        TextView title;
        TextView notes;
        TextView createdAT;
    }
}
