package com.example.jmeza.carrouselretrofit.api.base;

import com.squareup.otto.Bus;

/**
 * Created by jmeza on 28/03/2017.
 */

public class BusProvider {
    private static Bus bus;

    public static Bus getInstance() {
        if (bus != null) {
            return bus;
        }
        bus = new Bus();
        return bus;
    }
}
