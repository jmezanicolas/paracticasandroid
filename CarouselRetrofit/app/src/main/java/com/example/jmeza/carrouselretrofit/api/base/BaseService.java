package com.example.jmeza.carrouselretrofit.api.base;

import android.content.Context;

import com.example.jmeza.carrouselretrofit.R;

/**
 * Created by jmeza on 28/03/2017.
 */

public class BaseService {


    protected Context mContext;
    protected String mEndpoint;
    protected String mEndpointIA;
    public static String token;
    protected String mDelegacion;
    protected String mColonia;
    public static String mHayCobertura;
    public boolean f = false;


    public BaseService(Context context) {
        mContext = context;
        /**
         * Se configura con los parametros establecidos en el recurso services.xml
         */
        mEndpoint = "http://200.77.160.48:8001";//context.getString(R.string.main_endpoint);
        mEndpointIA = "http://izzi.iainteractive.mx";//context.getString(R.string.main_endpointIA);

    }

    public BaseService() {
    }

    public String getToken() {
        return token;
    }

    public BaseService(String token) {
        this.token = token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public String getmEndpoint() {
        return mEndpoint;
    }

    public void setmEndpoint(String mEndpoint) {
        this.mEndpoint = mEndpoint;
    }


}
