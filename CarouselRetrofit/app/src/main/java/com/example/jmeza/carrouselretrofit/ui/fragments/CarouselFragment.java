package com.example.jmeza.carrouselretrofit.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.jmeza.carrouselretrofit.R;
import com.example.jmeza.carrouselretrofit.ui.adapter.ViewPagerAdapter;
import com.example.jmeza.carrouselretrofit.api.base.BusProvider;
import com.example.jmeza.carrouselretrofit.api.events.GetPromotionEvent;
import com.example.jmeza.carrouselretrofit.api.events.SendPromotionEvent;
import com.example.jmeza.carrouselretrofit.api.model.PromotionResponse;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;


/**
 * A simple {@link Fragment} subclass.
 */
public class CarouselFragment extends Fragment implements View.OnClickListener {
    private ArrayList<PromotionResponse.response.result> promoLoginList = new ArrayList<>();

    private FragmentStatePagerAdapter adapter;
    ViewPager viewPager;
    Button btnSiguiente;

    CircleIndicator indicator;

    public static CarouselFragment newInstance() {
        return new CarouselFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_carousel, container, false);
        viewPager = (ViewPager) v.findViewById(R.id.view_pager);
        btnSiguiente = (Button) v.findViewById(R.id.botonSiguiente);
        indicator = (CircleIndicator) v.findViewById(R.id.indicator);
        BusProvider.getInstance().register(this);
        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().post(new GetPromotionEvent("000"));
    }

    @Subscribe
    public void onPromociones(SendPromotionEvent promoLogic) {
        if (promoLogic.isSuccess()) {
            promoLoginList = (ArrayList<PromotionResponse.response.result>) promoLogic.getPromotionResponse().getResponse().getResult();
            adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), promoLoginList);
            viewPager.setAdapter(adapter);
            indicator.setViewPager(viewPager);
        } else {
            Toast.makeText(getActivity(), promoLogic.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(getActivity(), "Botón", Toast.LENGTH_SHORT).show();
   /*     getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.baseContenedor, BienvenidoFragment.newInstance())
                .addToBackStack(null)
                .commit();*/
    }

}
