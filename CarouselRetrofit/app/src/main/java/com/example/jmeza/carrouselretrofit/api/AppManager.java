package com.example.jmeza.carrouselretrofit.api;

import com.example.jmeza.carrouselretrofit.api.Application.MyApplication;
import com.example.jmeza.carrouselretrofit.api.base.BaseEvent;
import com.example.jmeza.carrouselretrofit.api.base.EventProcessor;
import com.example.jmeza.carrouselretrofit.api.processors.PromotionProcessor;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * Created by jmeza on 28/03/2017.
 */

public class AppManager {
    private MyApplication mApplication;
    private ArrayList<EventProcessor> mProcessor = new ArrayList<>();

    public AppManager(MyApplication application) {
        this.mApplication = application;
        initProcess();
    }

    private void initProcess() {
        mProcessor.add(new PromotionProcessor());
    }

    @Subscribe
    public void onEvent(BaseEvent event) {
        EventProcessor processor = getProcessor(event);
        if (processor != null) {
            processor.process(mApplication, event);
        }
    }


    private EventProcessor getProcessor(BaseEvent event) {
        for (EventProcessor processor : mProcessor) {
            if (processor.canProcess(event)) {
                return processor;
            }
        }
        return null;
    }
}
