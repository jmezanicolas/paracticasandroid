package com.example.jmeza.carrouselretrofit.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.jmeza.carrouselretrofit.api.model.PromotionResponse;
import com.example.jmeza.carrouselretrofit.ui.fragments.PageFragment;

import java.util.ArrayList;

/**
 * Created by jmeza on 28/03/2017.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {


    private ArrayList<PromotionResponse.response.result> mPpromLogin;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<PromotionResponse.response.result> promLogin) {
        super(fm);
        this.mPpromLogin = promLogin;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.getInstance(mPpromLogin.get(position).getImagen());
    }

    @Override
    public int getCount() {
        return mPpromLogin.size();
    }

}
