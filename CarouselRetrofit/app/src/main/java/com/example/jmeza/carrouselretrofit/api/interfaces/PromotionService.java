package com.example.jmeza.carrouselretrofit.api.interfaces;

import com.example.jmeza.carrouselretrofit.api.model.PromotionResponse;

import retrofit.Callback;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by jmeza on 28/03/2017.
 */

public interface PromotionService {
    @POST("/WSVeL/webservices/carrusel/")
    void getPromocionesLogin(@Query("rpt") String rpt, Callback<PromotionResponse> callback);
}
