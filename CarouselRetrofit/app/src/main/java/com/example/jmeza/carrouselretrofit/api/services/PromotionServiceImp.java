package com.example.jmeza.carrouselretrofit.api.services;

import android.content.Context;

import com.example.jmeza.carrouselretrofit.R;
import com.example.jmeza.carrouselretrofit.api.base.BaseService;
import com.example.jmeza.carrouselretrofit.api.interfaces.PromotionService;
import com.example.jmeza.carrouselretrofit.api.model.PromotionResponse;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by jmeza on 28/03/2017.
 */

public class PromotionServiceImp extends BaseService {
    private PromotionsListener mListener;
    private PromotionService mService;

    public PromotionServiceImp(Context context, PromotionsListener listener) {
        super(context);
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setConnectTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setWriteTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        mContext = context;
        mListener = listener;
        mService = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(mEndpoint)
                .build()
                .create(PromotionService.class);
    }

    public void success(String rpt) {
        mService.getPromocionesLogin(rpt, new retrofit.Callback<PromotionResponse>() {
            @Override
            public void success(PromotionResponse promLogin, Response response) {
                if (promLogin.getIzziError().equals("Success")) {
                    if (mListener != null)
                        mListener.onSuccess(promLogin);
                } else {
                    if (mListener != null)
                        mListener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (mListener != null) {
                    mListener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                }
            }
        });
    }

    public interface PromotionsListener {
        void onSuccess(PromotionResponse resp);

        void onFailure(String error);
    }
}
