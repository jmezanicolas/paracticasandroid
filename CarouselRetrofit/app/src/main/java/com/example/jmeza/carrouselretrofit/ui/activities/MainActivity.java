package com.example.jmeza.carrouselretrofit.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;

import com.example.jmeza.carrouselretrofit.R;
import com.example.jmeza.carrouselretrofit.api.base.BusProvider;
import com.example.jmeza.carrouselretrofit.ui.fragments.CarouselFragment;

import butterknife.Bind;

public class MainActivity extends AppCompatActivity {
    @Bind(R.id.baseContenedor)
    RelativeLayout mLayout;
    @Bind(R.id.my_awesome_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadCarousel();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
//        GoogleAnalytics.getInstance(BaseActivity.this).reportActivityStart(this);
    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
//        GoogleAnalytics.getInstance(BaseActivity.this).reportActivityStop(this);
    }

    private void loadCarousel() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.baseContenedor, CarouselFragment.newInstance())
                .commit();
    }
}
