package com.example.jmeza.carrouselretrofit.api.model;

import java.util.List;

/**
 * Created by jmeza on 28/03/2017.
 */

public class PromotionResponse {
    private String izziError;
    private String izziErrorCode;
    private response response;
    private String token;

    public String getIzziError() {
        return izziError;
    }

    public void setIzziError(String izziError) {
        this.izziError = izziError;
    }

    public String getIzziErrorCode() {
        return izziErrorCode;
    }

    public void setIzziErrorCode(String izziErrorCode) {
        this.izziErrorCode = izziErrorCode;
    }

    public response getResponse() {
        return response;
    }

    public void setResponse(response response) {
        this.response = response;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public static class response {

        private List<result> result;

        public List<result> getResult() {
            return result;
        }

        public void setResult(List<result> result) {
            this.result = result;
        }

        public static class result {
            private String clave;
            private String imagen;

            public String getClave() {
                return clave;
            }

            public void setClave(String clave) {
                this.clave = clave;
            }

            public String getImagen() {
                return imagen;
            }

            public void setImagen(String imagen) {
                this.imagen = imagen;
            }
        }

    }
}
