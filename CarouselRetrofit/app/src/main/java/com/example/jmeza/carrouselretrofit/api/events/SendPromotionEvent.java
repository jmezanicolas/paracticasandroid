package com.example.jmeza.carrouselretrofit.api.events;

import com.example.jmeza.carrouselretrofit.api.model.PromotionResponse;
import com.example.jmeza.carrouselretrofit.api.base.BaseEvent;

/**
 * Created by jmeza on 28/03/2017.
 */

public class SendPromotionEvent extends BaseEvent {
    private boolean isSuccess;
    private String message;
    private PromotionResponse promotionResponse;

    public SendPromotionEvent(boolean isSuccess, String message, PromotionResponse promotionResponse) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.promotionResponse = promotionResponse;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public PromotionResponse getPromotionResponse() {
        return promotionResponse;
    }
}
