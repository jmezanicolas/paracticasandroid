package com.example.jmeza.carrouselretrofit.api.processors;

import com.example.jmeza.carrouselretrofit.api.Application.MyApplication;
import com.example.jmeza.carrouselretrofit.api.services.PromotionServiceImp;
import com.example.jmeza.carrouselretrofit.api.base.BusProvider;
import com.example.jmeza.carrouselretrofit.api.base.EventProcessor;
import com.example.jmeza.carrouselretrofit.api.events.GetPromotionEvent;
import com.example.jmeza.carrouselretrofit.api.events.SendPromotionEvent;
import com.example.jmeza.carrouselretrofit.api.model.PromotionResponse;

/**
 * Created by jmeza on 28/03/2017.
 */

public class PromotionProcessor implements EventProcessor<GetPromotionEvent>, PromotionServiceImp.PromotionsListener {
    @Override
    public void process(MyApplication aplicacion, GetPromotionEvent event) {
        PromotionServiceImp service = new PromotionServiceImp(aplicacion, this);
        service.success(event.getRpt());
    }

    @Override
    public boolean canProcess(Object event) {
        return event instanceof GetPromotionEvent;
    }

    @Override
    public void onSuccess(PromotionResponse resp) {
        BusProvider.getInstance().post(new SendPromotionEvent(true, null, resp));
    }

    @Override
    public void onFailure(String error) {
        BusProvider.getInstance().post(new SendPromotionEvent(false, error, null));
    }
}
