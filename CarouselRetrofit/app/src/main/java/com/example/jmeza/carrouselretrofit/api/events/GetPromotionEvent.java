package com.example.jmeza.carrouselretrofit.api.events;

import com.example.jmeza.carrouselretrofit.api.base.BaseEvent;

/**
 * Created by jmeza on 28/03/2017.
 */

public class GetPromotionEvent extends BaseEvent {
    private String rpt;

    public GetPromotionEvent(String rpt) {
        this.rpt = rpt;
    }

    public String getRpt() {
        return rpt;
    }
}
