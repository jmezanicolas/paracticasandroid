package com.example.jmeza.carrouselretrofit.api.Application;

import android.app.Application;

import com.example.jmeza.carrouselretrofit.api.AppManager;
import com.example.jmeza.carrouselretrofit.api.base.BusProvider;

/**
 * Created by jmeza on 28/03/2017.
 */

public class MyApplication extends Application {
    private AppManager mManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mManager = new AppManager(this);
        BusProvider.getInstance().register(mManager);
    }

}
