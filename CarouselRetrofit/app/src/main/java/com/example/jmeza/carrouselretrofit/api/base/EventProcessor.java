package com.example.jmeza.carrouselretrofit.api.base;

import com.example.jmeza.carrouselretrofit.api.Application.MyApplication;

/**
 * Created by jmeza on 28/03/2017.
 */

public interface EventProcessor<T> {

    void process(MyApplication aplicacion, T event);

    boolean canProcess(Object event);
}
