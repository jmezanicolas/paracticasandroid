package com.example.jmeza.citieslist.Interface;

import com.example.jmeza.citieslist.Models.Cities;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by jmeza on 06/03/2017.
 */

public interface CitiesService {
    @GET("/Consumo.svc/json/ObtenerCiudadesPaises")
    Call<List<Cities>> getData(@Query("idsPaises") String idsPaises);
}
