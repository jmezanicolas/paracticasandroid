package com.example.jmeza.citieslist.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.jmeza.citieslist.Adapter.MyAdapter;
import com.example.jmeza.citieslist.Interface.CitiesService;
import com.example.jmeza.citieslist.R;
import com.example.jmeza.citieslist.Models.Cities;
import com.example.jmeza.citieslist.datos.SqLite;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private List<Cities> cities;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadJson();
        loadUi();
    }

    private void loadUi() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);

    }

    private void loadJson() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.cinepolis.com.mx/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        CitiesService restClient = retrofit.create(CitiesService.class);
        Call<List<Cities>> callRequest = restClient.getData("1,2,3,4,5,6,9,15");
        callRequest.enqueue(new Callback<List<Cities>>() {
            @Override
            public void onResponse(Call<List<Cities>> call, Response<List<Cities>> response) {

                switch (response.code()) {
                    case 200:
                        cities = response.body();
                        loadDataRecyclerView();
                        break;
                    case 401:
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<Cities>> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }

    private void loadDataRecyclerView() {
        mAdapter = new MyAdapter(cities, R.layout.recycler_view_item, MainActivity.this, new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Cities _cities, int position) {
                Toast.makeText(MainActivity.this, cities.get(position).getName() + " - " + position, Toast.LENGTH_SHORT).show();
                SqLite sql = new SqLite(MainActivity.this);
                sql.downloadSqLite(cities.get(position).getId());
                //deleteMovie(position);
            }
        });
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

}
