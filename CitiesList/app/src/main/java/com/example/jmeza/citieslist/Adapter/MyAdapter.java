package com.example.jmeza.citieslist.Adapter;

/**
 * Created by jmeza on 06/03/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jmeza.citieslist.R;
import com.example.jmeza.citieslist.Models.Cities;
import com.example.jmeza.citieslist.Utils.Images;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Packo
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Cities> cities;
    private int layout;
    private OnItemClickListener itemClickListener;
    private Context context;

    public MyAdapter(List<Cities> cities, int layout, Context context, OnItemClickListener listener) {
        this.cities = cities;
        this.layout = layout;
        this.itemClickListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        //context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(cities.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() {
        return cities != null ? cities.size() : 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName;
        public ImageView imageView;
        public TextView textViewCountry;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.textView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            textViewCountry = (TextView) itemView.findViewById(R.id.textViewCountry);

        }

        public void bind(final Cities cities, final OnItemClickListener listener) {
            textViewName.setText(cities.getCountry());
            textViewCountry.setText(cities.getName());
            Picasso.with(context).load(Images.setId(cities.getIdCountry())).fit().into(imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(cities, getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Cities cities, int position);
    }
}
