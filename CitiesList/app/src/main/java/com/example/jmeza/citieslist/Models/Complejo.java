package com.example.jmeza.citieslist.Models;

/**
 * Created by jmeza on 08/03/2017.
 */

public class Complejo {
    private int id;
    private int idComplejoVista;
    private int idCiudad;
    private String nombre;
    private Double latitud;
    private Double longitud;
    private int telefono;
    private String direccion;
    private String esCompraAnticipada;
    private String esReservaPermitida;
    private String urlSitio;
    private String transporte;
    private int merchId;
    private String esVentaAlimentos;

    public Complejo() {
    }

    public Complejo(int id, int idComplejoVista, int idCiudad, String nombre, Double latitud, Double longitud, int telefono, String direccion, String esCompraAnticipada, String esReservaPermitida, String urlSitio, String transporte, int merchId, String esVentaAlimentos) {
        this.id = id;
        this.idComplejoVista = idComplejoVista;
        this.idCiudad = idCiudad;
        this.nombre = nombre;
        this.latitud = latitud;
        this.longitud = longitud;
        this.telefono = telefono;
        this.direccion = direccion;
        this.esCompraAnticipada = esCompraAnticipada;
        this.esReservaPermitida = esReservaPermitida;
        this.urlSitio = urlSitio;
        this.transporte = transporte;
        this.merchId = merchId;
        this.esVentaAlimentos = esVentaAlimentos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdComplejoVista() {
        return idComplejoVista;
    }

    public void setIdComplejoVista(int idComplejoVista) {
        this.idComplejoVista = idComplejoVista;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEsCompraAnticipada() {
        return esCompraAnticipada;
    }

    public void setEsCompraAnticipada(String esCompraAnticipada) {
        this.esCompraAnticipada = esCompraAnticipada;
    }

    public String getEsReservaPermitida() {
        return esReservaPermitida;
    }

    public void setEsReservaPermitida(String esReservaPermitida) {
        this.esReservaPermitida = esReservaPermitida;
    }

    public String getUrlSitio() {
        return urlSitio;
    }

    public void setUrlSitio(String urlSitio) {
        this.urlSitio = urlSitio;
    }

    public String getTransporte() {
        return transporte;
    }

    public void setTransporte(String transporte) {
        this.transporte = transporte;
    }

    public int getMerchId() {
        return merchId;
    }

    public void setMerchId(int merchId) {
        this.merchId = merchId;
    }

    public String getEsVentaAlimentos() {
        return esVentaAlimentos;
    }

    public void setEsVentaAlimentos(String esVentaAlimentos) {
        this.esVentaAlimentos = esVentaAlimentos;
    }
}
