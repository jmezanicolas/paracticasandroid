package com.example.jmeza.citieslist.datos;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.example.jmeza.citieslist.Models.Complejo;
import com.example.jmeza.citieslist.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmeza on 07/03/2017.
 */

public class SqLite extends SQLiteOpenHelper {
    private final static String TAG = SqLite.class.getSimpleName();
    private static String RUTA_BD;
    private final Context contexto;
    private SQLiteDatabase bd;
    private static String RutaApi;
    private static String NOMBRE_BD = "Cinepolis.sqlite";
    private static String NOMBRE_BDTEMP = "Cinepolis_temp.sqlite";
    //SqliteInterface sqliteInterface;

    public SqLite(Context context) {
        super(context, NOMBRE_BD, null, 1);
        RUTA_BD = "/data/data/" + context.getPackageName() + "/databases/";
        this.contexto = context;
        RutaApi = context.getString(R.string.url_rutas_yelmo);
    }

    public void downloadSqLite(int id) {
        String url = String.format("http://api.cinepolis.com.mx/sqlite.aspx?idCiudad=%d", id);
        Ion.with(contexto)
                .load(url)
                .noCache()
                .setTimeout(10000)
                .write(new File(String.format("%s%s", RUTA_BD, NOMBRE_BDTEMP)))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File result) {
                        if (EsValido(NOMBRE_BDTEMP)) {
                            //obtenerComplejo();
                            //ObtenerRutas();
                        } else {
                            Log.e(TAG, "Error al obtener Base de datos", e);
                            //sqliteInterface.ObtenerSQLiteCompleto(new Resultado(false, new Excepcion(100)));
                        }
                    }
                });
    }

    private void ObtenerRutas() {
        Ion.with(contexto)
                .load(RutaApi)
                .setTimeout(30000)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray rutas) {
                        if (e == null) {
                            //removerColumnasId(RUTA_BD + NOMBRE_BDTEMP);
                            SQLiteDatabase db = SQLiteDatabase.openDatabase(RUTA_BD + NOMBRE_BDTEMP, null, SQLiteDatabase.OPEN_READWRITE);
                            try {
                                String crearTabla = "CREATE TABLE IF NOT EXISTS Rutas (Tipo VARCHAR(60), URL VARCHAR(250));";
                                db.execSQL(crearTabla);
                                for (JsonElement Ruta : rutas) {
                                    JsonObject ruta = Ruta.getAsJsonObject();
                                    String url = ruta.get("Url").getAsString();
                                    db.execSQL(String.format("INSERT INTO RUTAS VALUES ('%s','%s');", ruta.get("Tipo"), url));
                                }
                                db.close();

                                if (ReemplazaDB()) {
                                    /*
                                    contexto.getSharedPreferences(AplicationConstants.PREFERENCES_NAME, Context.MODE_PRIVATE)
                                            .edit().putLong(AplicationConstants.PREFERENCE_LAST_UPDATE,
                                            new Date().getTime())
                                            .commit();
                                    contexto.getContentResolver().notifyChange(DbContract.Cartelera.CONTENT_URI, null, false);
                                    contexto.getContentResolver().notifyChange(DbContract.Banner.CONTENT_URI, null, false);
                                    contexto.getContentResolver().notifyChange(DbContract.ProximosEstrenos.CONTENT_URI, null, false);
                                    contexto.getContentResolver().notifyChange(DbContract.PeliculaMuestra.CONTENT_URI, null, false);
                                    sqliteInterface.ObtenerSQLiteCompleto(new Resultado(true));

                                    ContentResolver resolver = contexto.getContentResolver();
                                    ContentProviderClient client = resolver.acquireContentProviderClient(DbContract.CONTENT_AUTHORITY);
                                    CinepolisProvider provider = (CinepolisProvider) client.getLocalContentProvider();
                                    if (provider != null) {
                                        provider.resetDatabase();
                                    }
                                    client.release();
                                    */

                                } else {
                                    /*sqliteInterface.ObtenerSQLiteCompleto(new Resultado(false, new Excepcion(100)));*/
                                }

                            } catch (SQLException eSQL) {
                                db.close();
                                /*sqliteInterface.ObtenerSQLiteCompleto(new Resultado(false, new Excepcion(100)));*/
                            }
                        } else {
                            /*sqliteInterface.ObtenerSQLiteCompleto(new Resultado(false, new Excepcion(100)));*/
                        }
                    }
                });
    }

    private boolean ReemplazaDB() {
        try {
            File actual = new File(ObtenerRuta());
            if (actual.exists()) {
                actual.delete();
            }

            File nueva = new File(RUTA_BD + NOMBRE_BDTEMP);
            if (nueva.exists())
                nueva.renameTo(actual);
        } catch (Exception ex) {
            Log.e(SqLite.class.getSimpleName(), "Error al remplazar la bd", ex);
            return false;
        }
        return true;
    }

    private String ObtenerRuta() {
        String ruta = RUTA_BD + NOMBRE_BD;
        return ruta;
    }

    private boolean EsValido(String nombreBD) {
        File ruta = null;
        try {
            ruta = new File(RUTA_BD);
            if (!ruta.exists()) {
                ruta.mkdirs();
            }
            File f = new File(RUTA_BD + nombreBD);
            bd = SQLiteDatabase.openDatabase(f.getPath(), null, SQLiteDatabase.OPEN_READONLY);
            obtenerComplejo();
            /*
            Cursor cursor = bd.rawQuery("SELECT count(name) FROM sqlite_master WHERE type='table'", null);
            cursor.moveToFirst();
            if (cursor.getInt(0) <= 7) {
                bd = null;
            }*/
        } catch (Exception e) {
            bd = null;
        }
        if (bd != null) {
            bd.close();
        }
        return bd != null;
    }

    private List<Complejo> obtenerComplejo() {

        List<Complejo> list = new ArrayList<>();

        if (bd != null) {
            Cursor c = bd.rawQuery("Select * from Complejo", null);
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    //int VIN = cursor.getInt(cursor.getColumnIndex("VIN"));
                    int id = c.getInt(c.getColumnIndex("Id"));
                    int idComplejoVista = c.getInt(c.getColumnIndex("IdComplejoVista"));
                    int idCiudad = c.getInt(c.getColumnIndex("IdCiudad"));
                    String nombre = c.getString(c.getColumnIndex("Nombre"));
                    Double latitud = c.getDouble(c.getColumnIndex("Latitud"));
                    Double longitud = c.getDouble(c.getColumnIndex("Longitud"));
                    int telefono = c.getInt(c.getColumnIndex("Telefono"));
                    String direccion = c.getString(c.getColumnIndex("Direccion"));
                    String esCompraAnticipada = c.getString(c.getColumnIndex("EsCompraAnticipada"));
                    String esReservaPermitida = c.getString(c.getColumnIndex("EsReservaPermitida"));
                    String urlSitio = c.getString(c.getColumnIndex("UrlSitio"));
                    String transporte = c.getString(c.getColumnIndex("Transporte"));
                    int merchId = c.getInt(c.getColumnIndex("MerchId"));
                    String esVentaAlimentos = c.getString(c.getColumnIndex("EsVentaAlimentos"));
                    list.add(new Complejo(id, idComplejoVista, idCiudad, nombre, latitud, longitud, telefono, direccion, esCompraAnticipada, esReservaPermitida, urlSitio, transporte, merchId, esVentaAlimentos));
                    c.moveToNext();
                }
            }
        }
        return list;

    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
