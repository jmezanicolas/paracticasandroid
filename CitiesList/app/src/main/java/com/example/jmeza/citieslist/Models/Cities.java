package com.example.jmeza.citieslist.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jmeza on 06/03/2017.
 */

public class Cities {
    @SerializedName("Estado")
    private String state;
    @SerializedName("Id")
    private int id;
    @SerializedName("IdEstado")
    private int idState;
    @SerializedName("IdPais")
    private int idCountry;
    @SerializedName("Latitud")
    private Double latitude;
    @SerializedName("Longitud")
    private Double length;
    @SerializedName("Nombre")
    private String name;
    @SerializedName("Pais")
    private String country;
    @SerializedName("Uris")
    private String uris;

    public Cities() {
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdState() {
        return idState;
    }

    public void setIdState(int idState) {
        this.idState = idState;
    }

    public int getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(int idCountry) {
        this.idCountry = idCountry;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUris() {
        return uris;
    }

    public void setUris(String uris) {
        this.uris = uris;
    }
}
