package com.example.jmeza.citieslist.Utils;

import com.example.jmeza.citieslist.R;

/**
 * Created by jmeza on 07/03/2017.
 */

public class Images {
    public static int setId(int id) {
        switch (id) {
            case 1:
                return R.drawable.mexico;
            case 2:
                return R.drawable.guatemala;
            case 3:
                return R.drawable.costa_rica;
            case 4:
                return R.drawable.panama;
            case 5:
                return R.drawable.el_salvador;
            case 6:
                return R.drawable.honduras;
            case 9:
                return R.drawable.colombia;
            case 15:
                return R.drawable.peru;
            default:
                return 0;
        }
    }
}
