package com.example.jmeza.ejemplomvp.ui.presenter;

import com.example.jmeza.ejemplomvp.data.api.bases.BusProvider;
import com.example.jmeza.ejemplomvp.data.api.events.GetUserDetailEvent;
import com.example.jmeza.ejemplomvp.data.api.events.SendUserDetailEvent;
import com.example.jmeza.ejemplomvp.ui.views.DetailView;
import com.squareup.otto.Subscribe;

/**
 * Created by jmeza on 04/04/2017.
 */

public class DetailPresenter extends BasePresenter {
    DetailView mView;

    public DetailPresenter(DetailView mView) {
        onResume();
        this.mView = mView;
    }

    public void getDetail(String rpt) {
        BusProvider.getInstance().post(new GetUserDetailEvent(rpt));
    }

    @Subscribe
    public void onDetail(SendUserDetailEvent send) {
        mView.hideProgressDialog();
        if (send.isSuccess()) {
            mView.onShowDetail(send.getUserDetailResponse());
        } else {
            mView.onLoadError(send.getMessage());
        }
    }
}
