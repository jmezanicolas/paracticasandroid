package com.example.jmeza.ejemplomvp.data.api.processor;

import com.example.jmeza.ejemplomvp.MyApplication;
import com.example.jmeza.ejemplomvp.data.api.bases.BusProvider;
import com.example.jmeza.ejemplomvp.data.api.bases.EventProcessor;
import com.example.jmeza.ejemplomvp.data.api.events.GetUsersEvent;
import com.example.jmeza.ejemplomvp.data.api.events.SendUsersEvent;
import com.example.jmeza.ejemplomvp.data.api.models.UsersResponse;
import com.example.jmeza.ejemplomvp.data.api.services.UsersServiceImp;

import java.util.List;

/**
 * Created by jmeza on 30/03/2017.
 */

public class UsersProcessor implements EventProcessor<GetUsersEvent>, UsersServiceImp.UsersListener {
    @Override
    public void process(MyApplication aplicacion, GetUsersEvent event) {
        UsersServiceImp service = new UsersServiceImp(aplicacion, this);
        service.success();
    }

    @Override
    public boolean canProcess(Object event) {
        return event instanceof GetUsersEvent;
    }

    @Override
    public void onSuccess(List<UsersResponse> resp) {
        BusProvider.getInstance().post(new SendUsersEvent(true, null, resp));
    }

    @Override
    public void onFailure(String error) {
        BusProvider.getInstance().post(new SendUsersEvent(false, error, null));
    }
}
