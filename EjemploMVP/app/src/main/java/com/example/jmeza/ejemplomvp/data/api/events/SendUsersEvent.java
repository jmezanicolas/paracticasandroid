package com.example.jmeza.ejemplomvp.data.api.events;

import com.example.jmeza.ejemplomvp.data.api.bases.BaseEvent;
import com.example.jmeza.ejemplomvp.data.api.models.UsersResponse;

import java.util.List;

/**
 * Created by jmeza on 30/03/2017.
 */

public class SendUsersEvent extends BaseEvent {
    private boolean isSuccess;
    private String message;
    private List<UsersResponse> userResponse;

    public SendUsersEvent(boolean isSuccess, String message, List<UsersResponse> userResponse) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.userResponse = userResponse;
    }


    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public List<UsersResponse> getUserResponse() {
        return userResponse;
    }
}
