package com.example.jmeza.ejemplomvp.data.api.events;

import com.example.jmeza.ejemplomvp.data.api.bases.BaseEvent;
import com.example.jmeza.ejemplomvp.data.api.models.UserDetailResponse;

/**
 * Created by jmeza on 31/03/2017.
 */

public class SendUserDetailEvent extends BaseEvent {
    private boolean isSuccess;
    private String message;
    private UserDetailResponse userDetailResponse;

    public SendUserDetailEvent(boolean isSuccess, String message, UserDetailResponse userDetailResponse) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.userDetailResponse = userDetailResponse;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public UserDetailResponse getUserDetailResponse() {
        return userDetailResponse;
    }
}
