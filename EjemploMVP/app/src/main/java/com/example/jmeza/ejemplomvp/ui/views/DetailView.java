package com.example.jmeza.ejemplomvp.ui.views;

import android.content.Context;

import com.example.jmeza.ejemplomvp.data.api.models.UserDetailResponse;

/**
 * Created by jmeza on 04/04/2017.
 */

public interface DetailView extends BaseView {
    void onLoadError(String error);

    void onShowDetail(UserDetailResponse userDetail);

    Context getContext();
}
