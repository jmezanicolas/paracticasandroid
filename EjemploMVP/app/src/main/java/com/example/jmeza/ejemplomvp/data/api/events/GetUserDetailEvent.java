package com.example.jmeza.ejemplomvp.data.api.events;

import com.example.jmeza.ejemplomvp.data.api.bases.BaseEvent;

/**
 * Created by jmeza on 31/03/2017.
 */

public class GetUserDetailEvent extends BaseEvent {
    private String rpt;

    public GetUserDetailEvent(String rpt) {
        this.rpt = rpt;
    }

    public String getRpt() {
        return rpt;
    }
}
