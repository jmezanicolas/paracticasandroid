package com.example.jmeza.ejemplomvp.ui.presenter;

import com.example.jmeza.ejemplomvp.data.api.bases.BusProvider;
import com.example.jmeza.ejemplomvp.data.api.events.GetUsersEvent;
import com.example.jmeza.ejemplomvp.data.api.events.SendUsersEvent;
import com.example.jmeza.ejemplomvp.ui.views.BodyView;
import com.squareup.otto.Subscribe;

/**
 * Created by jmeza on 03/04/2017.
 */

public class BodyPresenter extends BasePresenter {
    BodyView mView;

    public BodyPresenter(BodyView mView) {
        onResume();
        this.mView = mView;
    }

    public void getBoddy(String rpt) {
        BusProvider.getInstance().post(new GetUsersEvent(""));
    }

    @Subscribe
    public void onUsers(SendUsersEvent sendEvent) {
        mView.hideProgressDialog();
        if (sendEvent.isSuccess()) {
            mView.onShowBody(sendEvent.getUserResponse());
        } else{
            mView.onLoadError(sendEvent.getMessage());
        }
    }
}
