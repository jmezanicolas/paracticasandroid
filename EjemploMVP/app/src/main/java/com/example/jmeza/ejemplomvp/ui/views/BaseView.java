package com.example.jmeza.ejemplomvp.ui.views;

import android.content.Context;

/**
 * Created by jmeza on 03/04/2017.
 */

public interface BaseView {

    void showProgressDialog();

    void hideProgressDialog();

    Context getContext();
}
