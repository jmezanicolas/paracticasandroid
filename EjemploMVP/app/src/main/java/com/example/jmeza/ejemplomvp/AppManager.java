package com.example.jmeza.ejemplomvp;

import com.example.jmeza.ejemplomvp.data.api.bases.BaseEvent;
import com.example.jmeza.ejemplomvp.data.api.bases.EventProcessor;
import com.example.jmeza.ejemplomvp.data.api.processor.UserDetailProcessor;
import com.example.jmeza.ejemplomvp.data.api.processor.UsersProcessor;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * Created by jmeza on 28/03/2017.
 */

public class AppManager {
    private MyApplication mApplication;
    private ArrayList<EventProcessor> mProcessor = new ArrayList<>();

    public AppManager(MyApplication application) {
        this.mApplication = application;
        initProcess();
    }

    private void initProcess() {
        mProcessor.add(new UsersProcessor());
        mProcessor.add(new UserDetailProcessor());
    }

    @Subscribe
    public void onEvent(BaseEvent event) {
        EventProcessor processor = getProcessor(event);
        if (processor != null) {
            processor.process(mApplication, event);
        }
    }


    private EventProcessor getProcessor(BaseEvent event) {
        for (EventProcessor processor : mProcessor) {
            if (processor.canProcess(event)) {
                return processor;
            }
        }
        return null;
    }


}
