package com.example.jmeza.ejemplomvp.data.api.interfaces;

import com.example.jmeza.ejemplomvp.data.api.models.UserDetailResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by jmeza on 31/03/2017.
 */

public interface UserDetailService {
    @GET("/users/{str}")
    void getUser(@Path("str") String str, Callback<UserDetailResponse> callback);
}
