package com.example.jmeza.ejemplomvp.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.jmeza.ejemplomvp.R;
import com.example.jmeza.ejemplomvp.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmeza on 27/03/2017.
 */

public class PageFragment extends BaseFragment {
    private String imageResource;

    public static PageFragment getInstance(String resourceID) {
        PageFragment f = new PageFragment();
        Bundle args = new Bundle();
        args.putString("image_source", resourceID);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageResource = getArguments().getString("image_source");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_page, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        Glide.with(getActivity())
                .load(imageResource)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //bitmap.recycle();
        //bitmap = null;
    }

}
