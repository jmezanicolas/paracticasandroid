package com.example.jmeza.ejemplomvp.ui.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.jmeza.ejemplomvp.R;
import com.example.jmeza.ejemplomvp.data.api.bases.BusProvider;
import com.example.jmeza.ejemplomvp.ui.fragments.BodyFragment;
import com.example.jmeza.ejemplomvp.ui.fragments.CarouselFragment;


public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        loadCarousel();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        BusProvider.getInstance().register(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    private void loadCarousel() {
        loadFragment(CarouselFragment.newInstance(), R.id.baseContenedor);

        loadFragment(BodyFragment.newInstance(), R.id.bodyBase);
    }

    private void loadFragment(Fragment fragment, int container) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(container, fragment)
                .commit();
    }
}
