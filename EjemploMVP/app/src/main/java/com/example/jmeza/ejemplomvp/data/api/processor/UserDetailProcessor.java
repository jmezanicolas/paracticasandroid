package com.example.jmeza.ejemplomvp.data.api.processor;

import com.example.jmeza.ejemplomvp.MyApplication;
import com.example.jmeza.ejemplomvp.data.api.bases.BusProvider;
import com.example.jmeza.ejemplomvp.data.api.bases.EventProcessor;
import com.example.jmeza.ejemplomvp.data.api.events.GetUserDetailEvent;
import com.example.jmeza.ejemplomvp.data.api.events.SendUserDetailEvent;
import com.example.jmeza.ejemplomvp.data.api.models.UserDetailResponse;
import com.example.jmeza.ejemplomvp.data.api.services.UserDetailServiceImp;

/**
 * Created by jmeza on 31/03/2017.
 */

public class UserDetailProcessor implements EventProcessor<GetUserDetailEvent>, UserDetailServiceImp.UserDetailListener {
    @Override
    public void process(MyApplication aplicacion, GetUserDetailEvent event) {
        UserDetailServiceImp service = new UserDetailServiceImp(aplicacion, this);
        service.success(event.getRpt());
    }

    @Override
    public boolean canProcess(Object event) {
        return event instanceof GetUserDetailEvent;
    }

    @Override
    public void onSuccess(UserDetailResponse resp) {
        BusProvider.getInstance().post(new SendUserDetailEvent(true, null, resp));
    }

    @Override
    public void onFailure(String error) {
        BusProvider.getInstance().post(new SendUserDetailEvent(false, error, null));
    }
}
