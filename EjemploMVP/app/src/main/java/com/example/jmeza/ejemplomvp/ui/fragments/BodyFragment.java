package com.example.jmeza.ejemplomvp.ui.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.jmeza.ejemplomvp.R;
import com.example.jmeza.ejemplomvp.data.api.models.UsersResponse;
import com.example.jmeza.ejemplomvp.ui.activities.DetailActivity;
import com.example.jmeza.ejemplomvp.ui.adapters.BodyAdapter;
import com.example.jmeza.ejemplomvp.ui.base.BaseFragment;
import com.example.jmeza.ejemplomvp.ui.presenter.BodyPresenter;
import com.example.jmeza.ejemplomvp.ui.views.BodyView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by jmeza on 30/03/2017.
 */

public class BodyFragment extends BaseFragment implements BodyView {
    private ProgressDialog mProgress;

    private List<UsersResponse> users = new ArrayList<>();
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private BodyPresenter mPressenter;

    public static BodyFragment newInstance() {
        return new BodyFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_body, container, false);

        mPressenter = new BodyPresenter(this);
        mLayoutManager = new LinearLayoutManager(getContext());
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPressenter.getBoddy("");
    }

    @Override
    public void onPause() {
        super.onPause();
        mPressenter.onPause();

    }

    @Override
    public void showProgressDialog() {
        mProgress = ProgressDialog.show(getContext(), null, "Cargando...", true);
    }

    @Override
    public void hideProgressDialog() {
        if (mProgress != null) {
            mProgress.dismiss();
        }
    }

    @Override
    public void onLoadError(String error) {
        if (getView() != null) {
            snackbar(getView(), error).show();
        }
    }

    @Override
    public void onShowBody(List<UsersResponse> usersResponse) {
        users = usersResponse;
        adapter = new BodyAdapter(getActivity(), users, R.layout.user_item,
                new BodyAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        Intent intent = new Intent(getActivity(), DetailActivity.class);
                        intent.putExtra("login", users.get(position).getLogin());
                        startActivity(intent);
                        getToast("Click");
                    }
                },
                new BodyAdapter.OnLongClickListener() {
                    @Override
                    public boolean onLongItemClick(int position) {
                        getToast("Click Long");
                        return true;
                    }
                }
        );
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(adapter);

    }

    private void getToast(String click) {
        Toast.makeText(getContext(), click, Toast.LENGTH_SHORT).show();
    }
}
