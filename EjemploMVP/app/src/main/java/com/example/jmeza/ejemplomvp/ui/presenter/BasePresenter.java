package com.example.jmeza.ejemplomvp.ui.presenter;

import com.example.jmeza.ejemplomvp.data.api.bases.BusProvider;
import com.squareup.otto.Bus;

/**
 * Created by jmeza on 03/04/2017.
 */

public class BasePresenter {
    protected Bus mBus;

    public BasePresenter() {
        mBus = BusProvider.getInstance();
    }

    public void onResume() {
        mBus.register(this);
    }

    public void onPause() {
        mBus.unregister(this);
    }
}
