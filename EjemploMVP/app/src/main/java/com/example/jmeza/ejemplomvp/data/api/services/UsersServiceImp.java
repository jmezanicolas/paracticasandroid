package com.example.jmeza.ejemplomvp.data.api.services;

import android.content.Context;

import com.example.jmeza.ejemplomvp.R;
import com.example.jmeza.ejemplomvp.data.api.bases.BaseService;
import com.example.jmeza.ejemplomvp.data.api.interfaces.UsersService;
import com.example.jmeza.ejemplomvp.data.api.models.UsersResponse;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by jmeza on 30/03/2017.
 */

public class UsersServiceImp extends BaseService {
    private UsersListener listener;
    private UsersService service;

    public UsersServiceImp(Context context, UsersListener usersListener) {
        super(context);
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setConnectTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setWriteTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        mContext = context;
        listener = usersListener;
        service = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(mGitHub)
                .build()
                .create(UsersService.class);
    }


    public void success() {

        service.getUsers(new retrofit.Callback<List<UsersResponse>>() {
            @Override
            public void success(List<UsersResponse> userResponses, Response response) {
                if (!userResponses.isEmpty()) {
                    if (listener != null)
                        listener.onSuccess(userResponses);
                } else {
                    if (listener != null) {
                        listener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                    }
                }
            }
            @Override
            public void failure(RetrofitError error) {
                if (listener != null) {
                    listener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                }
            }
        });
    }


    public interface UsersListener {
        void onSuccess(List<UsersResponse> resp);

        void onFailure(String error);

    }
}


