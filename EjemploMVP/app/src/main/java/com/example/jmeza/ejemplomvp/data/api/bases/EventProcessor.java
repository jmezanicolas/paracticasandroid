package com.example.jmeza.ejemplomvp.data.api.bases;


import com.example.jmeza.ejemplomvp.MyApplication;

/**
 * Created by jmeza on 28/03/2017.
 */

public interface EventProcessor<T> {

    void process(MyApplication aplicacion, T event);

    boolean canProcess(Object event);
}
