package com.example.jmeza.ejemplomvp.data.api.interfaces;

import com.example.jmeza.ejemplomvp.data.api.models.UsersResponse;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by jmeza on 30/03/2017.
 */

public interface UsersService {
    @GET("/users")
    void getUsers(Callback<List<UsersResponse>> callback);
}

