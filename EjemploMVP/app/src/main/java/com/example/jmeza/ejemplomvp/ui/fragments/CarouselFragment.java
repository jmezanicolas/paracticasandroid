package com.example.jmeza.ejemplomvp.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jmeza.ejemplomvp.R;
import com.example.jmeza.ejemplomvp.data.api.bases.BusProvider;
import com.example.jmeza.ejemplomvp.data.api.events.GetUsersEvent;
import com.example.jmeza.ejemplomvp.ui.adapters.ViewPagerAdapter;
import com.example.jmeza.ejemplomvp.ui.base.BaseFragment;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;


/**
 * A simple {@link Fragment} subclass.
 */
public class CarouselFragment extends BaseFragment {
    private ArrayList<String> slide = new ArrayList<>();

    private FragmentStatePagerAdapter adapter;
    ViewPager viewPager;

    CircleIndicator indicator;

    public static CarouselFragment newInstance() {
        return new CarouselFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_carousel, container, false);
        viewPager = (ViewPager) v.findViewById(R.id.view_pager);
        indicator = (CircleIndicator) v.findViewById(R.id.indicator);
        getAllSlide();
        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), slide);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        return v;
    }

    public void getAllSlide() {
        slide.add("http://photos1.blogger.com/x/blogger2/1191/623580003647996/400/881235/rober%20(46).jpg");
        slide.add("http://cidi.com/wp-content/uploads/2015/02/paisajes-irlanda-inmersion-400x100.jpg");
        slide.add("https://www.condortravel.com/static/media/tours/t/b09c72f735c6dd98167501a7aeb23637/Bariloche_tours.jpeg");
        slide.add("https://www.condortravel.com/static/media/tours/t/bfb71b199ddcfd52a54650454f32bfff/banner-web_1.jpeg");
        slide.add("https://www.condortravel.com/static/media/tours/t/376aebd855534568083cb0811efe95aa/Mendoza_Argentina_wine_tours.jpeg");
    }
}
