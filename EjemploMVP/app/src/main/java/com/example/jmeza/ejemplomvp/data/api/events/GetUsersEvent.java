package com.example.jmeza.ejemplomvp.data.api.events;

import com.example.jmeza.ejemplomvp.data.api.bases.BaseEvent;

/**
 * Created by jmeza on 30/03/2017.
 */

public class GetUsersEvent extends BaseEvent {
    private String rpt;

    public GetUsersEvent(String rpt) {
        this.rpt = rpt;
    }

    public String getRpt() {
        return rpt;
    }
}
