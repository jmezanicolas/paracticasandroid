package com.example.jmeza.ejemplomvp.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jmeza.ejemplomvp.R;
import com.example.jmeza.ejemplomvp.data.api.bases.BusProvider;
import com.example.jmeza.ejemplomvp.data.api.models.UserDetailResponse;
import com.example.jmeza.ejemplomvp.ui.presenter.DetailPresenter;
import com.example.jmeza.ejemplomvp.ui.views.DetailView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Packp-pc on 30/03/2017.
 */

public class DetailActivity extends AppCompatActivity implements View.OnClickListener, DetailView {
    private UserDetailResponse users = new UserDetailResponse();
    private String login;
    @Bind(R.id.imageViewDetail)
    ImageView imageViewDetail;
    @Bind(R.id.txtLogin)
    TextView txtLogin;
    @Bind(R.id.txtLocation)
    TextView txtLocation;
    @Bind(R.id.txtEmail)
    TextView txtEmail;
    @Bind(R.id.txtCompany)
    TextView txtCompany;
    @Bind(R.id.btnSiguiente)
    Button btnSig;

    private ProgressDialog mProgress;
    private DetailPresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitie_detail);
        ButterKnife.bind(this);
        mPresenter = new DetailPresenter(this);

        BusProvider.getInstance().register(this);
        login = getIntent().getExtras().getString("login").toString();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getDetail(login);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, ParallaxActivity.class);
        startActivity(intent);
    }


    @Override
    public void showProgressDialog() {
        mProgress = ProgressDialog.show(this, null, "Cargando", true);
    }

    @Override
    public void hideProgressDialog() {
        if (mProgress != null) {
            mProgress.dismiss();
        }
    }

    @Override
    public void onLoadError(String error) {
//        if (getView() != null) {
//            snackbar(getView(), error).show();
//        }
    }

    @Override
    public void onShowDetail(UserDetailResponse userDetail) {
        users = userDetail;
        setUi();
    }

    @Override
    public Context getContext() {
        return this;
    }

    private void setUi() {
        txtLogin.setText("Login: " + users.getLogin());
        txtLocation.setText("Locaion: " + users.getLocation());
        txtEmail.setText("Email: " + users.getEmail());
        txtCompany.setText("Company: " + users.getCompany());

        String url = users.getAvatarUrl();
        Glide.with(getApplicationContext())
                .load(url)
                .centerCrop()
                .into(imageViewDetail);
        btnSig.setOnClickListener(this);
    }
}
