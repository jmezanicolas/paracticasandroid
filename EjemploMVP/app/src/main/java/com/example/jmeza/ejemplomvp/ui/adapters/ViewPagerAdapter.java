package com.example.jmeza.ejemplomvp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.jmeza.ejemplomvp.ui.fragments.PageFragment;

import java.util.ArrayList;

/**
 * Created by jmeza on 28/03/2017.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {


    private ArrayList<String> mSlide;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<String> slide) {
        super(fm);
        this.mSlide = slide;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.getInstance(mSlide.get(position));
    }

    @Override
    public int getCount() {
        return mSlide.size();
    }

}
