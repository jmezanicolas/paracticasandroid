package com.example.jmeza.ejemplomvp.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.jmeza.ejemplomvp.R;
import com.example.jmeza.ejemplomvp.data.api.models.UsersResponse;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by jmeza on 30/03/2017.
 */

public class BodyAdapter extends RecyclerView.Adapter<BodyAdapter.ViewHolder> {

    private List<UsersResponse> users = new ArrayList<>();
    private int layout;
    private OnItemClickListener itemClickListener;
    private OnLongClickListener onLongClickListener;
    private Context context;

    public BodyAdapter(Context context, List<UsersResponse> users, int layout,
                       OnItemClickListener itemClickListener,
                       OnLongClickListener onLongClickListener) {
        this.users = users;
        this.layout = layout;
        this.itemClickListener = itemClickListener;
        this.onLongClickListener = onLongClickListener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UsersResponse item = users.get(position);
        holder.bind(item, itemClickListener);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtViewName;
        public TextView txtViewType;
        public ImageView imageLogin;

        public ViewHolder(View view) {
            super(view);
            txtViewName = (TextView) view.findViewById(R.id.txtViewName);
            imageLogin = (ImageView) view.findViewById(R.id.imageLogin);
            txtViewType = (TextView) view.findViewById(R.id.txtViewType);
        }

        public void bind(final UsersResponse userResponse, final OnItemClickListener listener) {
            txtViewName.setText("User: " + userResponse.getLogin());
            txtViewType.setText("login: " + userResponse.getType());
            Glide.with(context)
                    .load(userResponse.getAvatarUrl())
                    .crossFade()
                    .into(imageLogin);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return onLongClickListener.onLongItemClick(getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemClickListener {

        void onItemClick(int position);
    }

    public interface OnLongClickListener {
        boolean onLongItemClick(int position);
    }
}
