package com.example.jmeza.ejemplomvp.data.api.services;

import android.content.Context;

import com.example.jmeza.ejemplomvp.R;
import com.example.jmeza.ejemplomvp.data.api.bases.BaseService;
import com.example.jmeza.ejemplomvp.data.api.interfaces.UserDetailService;
import com.example.jmeza.ejemplomvp.data.api.models.UserDetailResponse;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by jmeza on 31/03/2017.
 */

public class UserDetailServiceImp extends BaseService {
    private UserDetailServiceImp.UserDetailListener listener;
    private UserDetailService service;

    public UserDetailServiceImp(Context context, UserDetailServiceImp.UserDetailListener usersListener) {
        super(context);
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setConnectTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setWriteTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        mContext = context;
        listener = usersListener;
        service = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(mGitHub)
                .build()
                .create(UserDetailService.class);
    }


    public void success(String rpt) {
        service.getUser(rpt, new retrofit.Callback<UserDetailResponse>() {
            @Override
            public void success(UserDetailResponse userResp, Response response) {
                if (!userResp.getLogin().equals("")) {
                    if (listener != null)
                        listener.onSuccess(userResp);
                } else {
                    if (listener != null) {
                        listener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (listener != null) {
                    listener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                }
            }
        });
    }

    public interface UserDetailListener {
        void onSuccess(UserDetailResponse resp);

        void onFailure(String error);

    }
}

