package com.example.jmeza.ejemplomvp;

import android.app.Application;

import com.example.jmeza.ejemplomvp.data.api.bases.BusProvider;


/**
 * Created by jmeza on 28/03/2017.
 */

public class MyApplication extends Application {
    private AppManager mManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mManager = new AppManager(this);
        BusProvider.getInstance().register(mManager);
    }

}
