package com.example.jmeza.ejemplomvp.ui.views;

import android.content.Context;

import com.example.jmeza.ejemplomvp.data.api.models.UsersResponse;

import java.util.List;

/**
 * Created by jmeza on 03/04/2017.
 */

public interface BodyView extends BaseView {
    void onLoadError(String error);

    void onShowBody(List<UsersResponse> usersResponse);

    Context getContext();
}
