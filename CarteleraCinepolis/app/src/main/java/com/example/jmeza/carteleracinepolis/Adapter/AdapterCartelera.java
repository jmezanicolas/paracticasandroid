package com.example.jmeza.carteleracinepolis.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jmeza.carteleracinepolis.Models.Pelicula;
import com.example.jmeza.carteleracinepolis.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jmeza on 09/03/2017.
 */

public class AdapterCartelera extends RecyclerView.Adapter<AdapterCartelera.ViewHolder> {
    private List<Pelicula> pelicula;
    private int layout;
    private AdapterCartelera.OnItemClickListener itemClickListener;
    private Context context;

    public AdapterCartelera(List<Pelicula> pelicula, int layout, Context context, AdapterCartelera.OnItemClickListener listener) {
        this.pelicula = pelicula;
        this.layout = layout;
        this.itemClickListener = listener;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        AdapterCartelera.ViewHolder vh = new AdapterCartelera.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(pelicula.get(position), itemClickListener);
    }


    @Override
    public int getItemCount() {
        return pelicula.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName;
        public ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.textViewNombre);
            imageView = (ImageView) itemView.findViewById(R.id.imageViewCart);

        }

        public void bind(final Pelicula pelicula, final OnItemClickListener listener) {
            textViewName.setText(pelicula.getTitulo());
            String resolution = context.getString(R.string.density_low);
            Picasso.with(context).load("http://cinepolis.com/_MOVIL/Android/cartel/xxhdpi/" + pelicula.getImagenCartel()).into(imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(pelicula, getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Pelicula pelicula, int position);
    }
}
