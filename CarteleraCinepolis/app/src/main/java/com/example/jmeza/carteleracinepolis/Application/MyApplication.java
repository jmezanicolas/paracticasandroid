package com.example.jmeza.carteleracinepolis.Application;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by jmeza on 07/03/2017.
 */

public class MyApplication extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
