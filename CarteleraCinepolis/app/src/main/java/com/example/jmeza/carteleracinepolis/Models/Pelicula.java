package com.example.jmeza.carteleracinepolis.Models;

/**
 * Created by jmeza on 09/03/2017.
 */

public class Pelicula {
    private int id;
    private String titulo;
    private String tituloOriginal;
    private String genero;
    private String clasificacion;
    private String duracion;
    private String director;
    private String actores;
    private String sinopsis;
    private String imagenCartel;
    private String sello;
    private String selloStatus;
    private String clave;

    public Pelicula() {
    }

    public Pelicula(int id, String titulo, String tituloOriginal, String genero, String clasificacion, String duracion, String director, String actores, String sinopsis, String imagenCartel, String sello, String selloStatus, String clave) {
        this.id = id;
        this.titulo = titulo;
        this.tituloOriginal = tituloOriginal;
        this.genero = genero;
        this.clasificacion = clasificacion;
        this.duracion = duracion;
        this.director = director;
        this.actores = actores;
        this.sinopsis = sinopsis;
        this.imagenCartel = imagenCartel;
        this.sello = sello;
        this.selloStatus = selloStatus;
        this.clave = clave;
    }

    public Pelicula(int id, String titulo, String tituloOriginal, String imagenCartel) {
        this.id = id;
        this.titulo = titulo;
        this.tituloOriginal = tituloOriginal;
        this.imagenCartel = imagenCartel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTituloOriginal() {
        return tituloOriginal;
    }

    public void setTituloOriginal(String tituloOriginal) {
        this.tituloOriginal = tituloOriginal;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActores() {
        return actores;
    }

    public void setActores(String actores) {
        this.actores = actores;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getImagenCartel() {
        return imagenCartel;
    }

    public void setImagenCartel(String imagenCartel) {
        this.imagenCartel = imagenCartel;
    }

    public String getSello() {
        return sello;
    }

    public void setSello(String sello) {
        this.sello = sello;
    }

    public String getSelloStatus() {
        return selloStatus;
    }

    public void setSelloStatus(String selloStatus) {
        this.selloStatus = selloStatus;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
