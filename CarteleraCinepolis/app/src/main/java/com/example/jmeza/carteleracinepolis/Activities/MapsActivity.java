package com.example.jmeza.carteleracinepolis.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.example.jmeza.carteleracinepolis.Models.Complejo;
import com.example.jmeza.carteleracinepolis.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private ArrayList<Complejo> complejos;
    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this);
        cargarUbicacion();

    }

    private void cargarUbicacion() {
        if (getIntent().getExtras() != null) {
            complejos = (ArrayList<Complejo>) getIntent().getSerializableExtra("complejos");
        }

        LatLng[] latLngsArr = new LatLng[complejos.size()];
        for (int a = 0; a < complejos.size(); a++) {
            latLngsArr[a] = new LatLng(complejos.get(a).getLatitud(), complejos.get(a).getLongitud());
            Marker melbourne =
                    mMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.mipmap.ic_cinepolis))
                            .position(latLngsArr[a])
                            .title(complejos.get(a).getNombre())
                            .snippet(complejos.get(a).getDireccion()));
            melbourne.showInfoWindow();
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngsArr[a]));
        }
        mMap.moveCamera(CameraUpdateFactory.zoomTo(12));
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        int complejoId = getId(marker);
        Toast.makeText(MapsActivity.this, "Soy un toast cholo", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MapsActivity.this, CarteleraActivity.class);
        intent.putExtra("complejoId", complejoId);
        startActivity(intent);
    }

    private int getId(Marker marker) {
        int id = 0;
        Double lat = marker.getPosition().latitude;
        Double lng = marker.getPosition().longitude;
        for (int a = 0; a < complejos.size(); a++) {
            if (complejos.get(a).getLatitud().equals(lat) && complejos.get(a).getLongitud().equals(lng)) {
                id = complejos.get(0).getId();
            }
        }
        return id;
    }
}
