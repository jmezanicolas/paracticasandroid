package com.example.jmeza.carteleracinepolis.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jmeza.carteleracinepolis.R;

public class SinopsisFragment extends Fragment {


    public SinopsisFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sinopsis, container, false);
        return view;
    }

    public interface DataListener {
        void sendData(String texo);
    }

}
