package com.example.jmeza.carteleracinepolis.datos;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;

import com.example.jmeza.carteleracinepolis.Activities.MapsActivity;
import com.example.jmeza.carteleracinepolis.Models.Complejo;
import com.example.jmeza.carteleracinepolis.R;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by jmeza on 07/03/2017.
 */

public class SqLite extends SQLiteOpenHelper {
    private final static String TAG = SqLite.class.getSimpleName();
    public static String RUTA_BD;
    private final Context contexto;
    private SQLiteDatabase bd;
    private static String RutaApi;
    public static String NOMBRE_BD = "Cinepolis.sqlite";
    public static String NOMBRE_BDTEMP = "Cinepolis_temp.sqlite";
    private ArrayList<Complejo> complejos = null;

    public SqLite(Context context) {
        super(context, NOMBRE_BD, null, 1);
        RUTA_BD = "/data/data/" + context.getPackageName() + "/databases/";
        this.contexto = context;
        RutaApi = context.getString(R.string.url_rutas_yelmo);
    }

    public void downloadSqLite(int id) {
        String url = String.format("http://api.cinepolis.com.mx/sqlite.aspx?idCiudad=%d", id);
        Ion.with(contexto)
                .load(url)
                .noCache()
                .setTimeout(10000)
                .write(new File(String.format("%s%s", RUTA_BD, NOMBRE_BDTEMP)))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File result) {
                        if (EsValido(NOMBRE_BDTEMP)) {
                            Intent intent = new Intent(contexto, MapsActivity.class);
                            Bundle info = new Bundle();
                            info.putSerializable("complejos", complejos);
                            intent.putExtras(info);
                            contexto.startActivity(intent);
                        } else {
                            Log.e(TAG, "Error al obtener Base de datos", e);
                        }
                    }
                });
    }

    private boolean EsValido(String nombreBD) {
        File ruta = null;
        try {
            ruta = new File(RUTA_BD);
            if (!ruta.exists()) {
                ruta.mkdirs();
            }
            File f = new File(RUTA_BD + nombreBD);
            bd = SQLiteDatabase.openDatabase(f.getPath(), null, SQLiteDatabase.OPEN_READONLY);
            complejos = obtenerComplejo();

        } catch (Exception e) {
            bd = null;
        }
        if (bd != null) {
            bd.close();
        }
        return bd != null;
    }

    private ArrayList<Complejo> obtenerComplejo() {
        ArrayList<Complejo> list = new ArrayList<>();
        if (bd != null) {
            Cursor c = bd.rawQuery("Select * from Complejo", null);
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    int id = c.getInt(c.getColumnIndex("Id"));
                    int idComplejoVista = c.getInt(c.getColumnIndex("IdComplejoVista"));
                    int idCiudad = c.getInt(c.getColumnIndex("IdCiudad"));
                    String nombre = c.getString(c.getColumnIndex("Nombre"));
                    Double latitud = c.getDouble(c.getColumnIndex("Latitud"));
                    Double longitud = c.getDouble(c.getColumnIndex("Longitud"));
                    int telefono = c.getInt(c.getColumnIndex("Telefono"));
                    String direccion = c.getString(c.getColumnIndex("Direccion"));
                    String esCompraAnticipada = c.getString(c.getColumnIndex("EsCompraAnticipada"));
                    String esReservaPermitida = c.getString(c.getColumnIndex("EsReservaPermitida"));
                    String urlSitio = c.getString(c.getColumnIndex("UrlSitio"));
                    String transporte = c.getString(c.getColumnIndex("Transporte"));
                    int merchId = c.getInt(c.getColumnIndex("MerchId"));
                    String esVentaAlimentos = c.getString(c.getColumnIndex("EsVentaAlimentos"));
                    list.add(new Complejo(id, idComplejoVista, idCiudad, nombre, latitud, longitud, telefono, direccion, esCompraAnticipada, esReservaPermitida, urlSitio, transporte, merchId, esVentaAlimentos));
                    c.moveToNext();
                }
            }
        }
        return list;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
