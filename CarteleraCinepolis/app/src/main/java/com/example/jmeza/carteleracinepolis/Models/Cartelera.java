package com.example.jmeza.carteleracinepolis.Models;

/**
 * Created by jmeza on 09/03/2017.
 */

public class Cartelera {
    private int id;
    private int idPelicula;
    private int idComplejoVista;
    private int idComplejo;
    private int idPeliculaVista;
    private int idShowTime;
    private String horario;
    private String fecha;
    private String sello;
    private int sala;

    public Cartelera() {
    }

    public Cartelera(int id, int idPelicula, int idComplejoVista, int idComplejo, int idPeliculaVista, int idShowTime, String horario, String fecha, String sello, int sala) {
        this.id = id;
        this.idPelicula = idPelicula;
        this.idComplejoVista = idComplejoVista;
        this.idComplejo = idComplejo;
        this.idPeliculaVista = idPeliculaVista;
        this.idShowTime = idShowTime;
        this.horario = horario;
        this.fecha = fecha;
        this.sello = sello;
        this.sala = sala;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(int idPelicula) {
        this.idPelicula = idPelicula;
    }

    public int getIdComplejoVista() {
        return idComplejoVista;
    }

    public void setIdComplejoVista(int idComplejoVista) {
        this.idComplejoVista = idComplejoVista;
    }

    public int getIdComplejo() {
        return idComplejo;
    }

    public void setIdComplejo(int idComplejo) {
        this.idComplejo = idComplejo;
    }

    public int getIdPeliculaVista() {
        return idPeliculaVista;
    }

    public void setIdPeliculaVista(int idPeliculaVista) {
        this.idPeliculaVista = idPeliculaVista;
    }

    public int getIdShowTime() {
        return idShowTime;
    }

    public void setIdShowTime(int idShowTime) {
        this.idShowTime = idShowTime;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getSello() {
        return sello;
    }

    public void setSello(String sello) {
        this.sello = sello;
    }

    public int getSala() {
        return sala;
    }

    public void setSala(int sala) {
        this.sala = sala;
    }
}
