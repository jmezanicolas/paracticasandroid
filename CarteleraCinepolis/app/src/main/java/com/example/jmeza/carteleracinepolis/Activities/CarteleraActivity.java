package com.example.jmeza.carteleracinepolis.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.example.jmeza.carteleracinepolis.Adapter.AdapterCartelera;
import com.example.jmeza.carteleracinepolis.Models.Pelicula;
import com.example.jmeza.carteleracinepolis.R;
import com.example.jmeza.carteleracinepolis.datos.SqLite;

import java.util.ArrayList;
import java.util.List;

public class CarteleraActivity extends AppCompatActivity {
    private List<Pelicula> pelicula;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private SQLiteDatabase bd;
    private RecyclerView.LayoutManager mLayoutManager;
    private int complejoId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartelera);
        if (getIntent().getExtras() != null) {
            complejoId = getIntent().getExtras().getInt("complejoId");
        }
        loadUi();
        loadDataRecyclerView();
    }

    private void loadUi() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewCartelera);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager = new GridLayoutManager(this, 3);
        mLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
    }

    private void getAllCartelera() {
        SqLite sql = new SqLite(CarteleraActivity.this);
        String ruta = SqLite.RUTA_BD + SqLite.NOMBRE_BDTEMP;
        try {
            bd = SQLiteDatabase.openDatabase(ruta, null, SQLiteDatabase.OPEN_READONLY);
            pelicula = getData();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private ArrayList<Pelicula> getData() {
        ArrayList<Pelicula> list = new ArrayList<>();
        try {
            if (bd != null) {
                String[] args = new String[]{String.valueOf(complejoId)};

                Cursor c = bd.rawQuery("selecT DISTINCT pel.id, pel.Titulo, pel.TituloOriginal, pel.ImagenCartel  from cartelera car inner join pelicula pel where car.idPelicula = pel.id and car.idComplejo=?", args);
                //Cursor c = bd.query("cartelera", campos, "IdComplejo=?", args, null, null, null);
                //Cursor c = bd.rawQuery("Select * from cartelera", null);
                if (c.moveToFirst()) {
                    while (!c.isAfterLast()) {
                        int id = c.getInt(c.getColumnIndex("Id"));
                        String titulo = c.getString(c.getColumnIndex("Titulo"));
                        String tituloOriginal = c.getString(c.getColumnIndex("TituloOriginal"));
                        String imagenCartel = c.getString(c.getColumnIndex("ImagenCartel"));
                        list.add(new Pelicula(id, titulo, tituloOriginal, imagenCartel));
                        c.moveToNext();
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    private void loadDataRecyclerView() {
        getAllCartelera();
        mAdapter = new AdapterCartelera(pelicula, R.layout.recycler_view_cartelera, CarteleraActivity.this, new AdapterCartelera.OnItemClickListener() {
            @Override
            public void onItemClick(Pelicula _pelicula, int position) {
                Intent intent = new Intent(CarteleraActivity.this, HorariosActivity.class);
                intent.putExtra("title", pelicula.get(position).getTitulo());
                intent.putExtra("id", pelicula.get(position).getId());
                startActivity(intent);
            }
        });

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }
}
