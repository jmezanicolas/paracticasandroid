package com.example.jmeza.carteleracinepolis.Adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.jmeza.carteleracinepolis.Fragments.GaleriaFragment;
import com.example.jmeza.carteleracinepolis.Fragments.HorariosFragment;
import com.example.jmeza.carteleracinepolis.Fragments.SinopsisFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private int numberOfTabs;

    public PagerAdapter(FragmentManager fm, int numberOfTabs) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HorariosFragment();
            case 1:
                return new SinopsisFragment();
            case 2:
                return new GaleriaFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
