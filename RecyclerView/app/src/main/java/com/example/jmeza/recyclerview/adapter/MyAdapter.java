package com.example.jmeza.recyclerview.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jmeza.recyclerview.R;
import com.example.jmeza.recyclerview.models.Movies;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Packo
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Movies> movies;
    private int layout;
    private OnItemClickListener itemClickListener;
    private OnLongClickListener onLongClickListener;
    private Context context;

    public MyAdapter(List<Movies> movies, int layout, Context context, OnItemClickListener listener, OnLongClickListener onLongClickListener) {
        this.movies = movies;
        this.layout = layout;
        this.itemClickListener = listener;
        this.onLongClickListener = onLongClickListener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        //context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Movies item = movies.get(position);
        holder.bind(item, itemClickListener);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName;
        public ImageView imageView;
        public TextView textViewYear;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.textView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            textViewYear = (TextView) itemView.findViewById(R.id.textViewYear);

        }

        public void bind(final Movies movie, final OnItemClickListener listener) {
            textViewName.setText(movie.getName());
            textViewYear.setText(String.valueOf(movie.getYear()));
            Picasso.with(context).load(movie.getImage()).fit().into(imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(movie.getSelect() ? false : true, getAdapterPosition());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return onLongClickListener.onLongItemClick(movie.getSelect() ? false : true, getAdapterPosition());
                }
            });

            itemView.setBackgroundColor(Color.TRANSPARENT);
            if (movie.getSelect()) {
                itemView.setBackgroundColor(Color.CYAN);
            }
        }
    }

    public interface OnItemClickListener {

        void onItemClick(boolean selected, int position);
    }

    public interface OnLongClickListener {
        boolean onLongItemClick(boolean selected, int position);
    }
}


