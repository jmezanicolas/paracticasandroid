package com.example.jmeza.recyclerview.models;

/**
 * Created by jmeza on 28/02/2017.
 */

public class Movies {
    private String name;
    private int image;
    private int year;
    private boolean select;

    public Movies() {
    }

    public Movies(String name, int image, int year) {
        this.name = name;
        this.image = image;
        this.year = year;
        this.select = false;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean getSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}
