package com.example.jmeza.recyclerview.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.jmeza.recyclerview.R;
import com.example.jmeza.recyclerview.adapter.MyAdapter;
import com.example.jmeza.recyclerview.models.Movies;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Movies> movies;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int count = 0;
    private Menu mMenu;
    private boolean band = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        movies = this.getAllMovies();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        /*mLayoutManager = new GridLayoutManager(this, 2);
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);*/


        mAdapter = new MyAdapter(movies, R.layout.recycler_view_item, MainActivity.this,
                new MyAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(boolean selected, int position) {
                        if (band) {
                            movies.get(position).setSelect(selected);
                            showMenu();

                        }
                        if (getSelectedItem() == 0) {
                            hideMenu();
                            band = false;
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                },
                new MyAdapter.OnLongClickListener() {
                    @Override
                    public boolean onLongItemClick(boolean selected, int position) {
                        movies.get(position).setSelect(selected);
                        mAdapter.notifyDataSetChanged();
                        band = true;
                        showMenu();
                        return true;
                    }
                }
        );
        recyclerLoad();
    }

    private void showMenu() {
        MenuItem menu = mMenu.findItem(R.id.delete_item);
        if (!menu.isVisible()) {
            menu.setVisible(true);
        }
    }

    private void hideMenu() {
        MenuItem menu = mMenu.findItem(R.id.delete_item);
        if (menu.isVisible()) {
            menu.setVisible(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_name:
                this.addMovie(0);
                return true;
            case R.id.delete_item:
                this.removeItems(item);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void removeItems(MenuItem mItem) {
        List<Movies> tmpList = new ArrayList<>();
        tmpList.addAll(movies);
        for (Movies item : movies) {
            if (item.getSelect()) {
                tmpList.remove(item);
            }
        }
        movies.clear();
        movies.addAll(tmpList);
        mItem.setVisible(false);
        mAdapter.notifyDataSetChanged();
    }

    private void addMovie(int position) {
        movies.add(position, new Movies("Nueva pelicula " + (++count), 2016, R.drawable.one));
        mAdapter.notifyItemInserted(position);
        mLayoutManager.scrollToPosition(position);
    }

    private int getSelectedItem() {
        int selected = 0;
        for (Movies mov : movies) {
            if (mov.getSelect()) {
                selected++;
            }
        }
        return selected;
    }

    private List<Movies> getAllMovies() {
        return new ArrayList<Movies>() {{
            add(new Movies("Capitan America", R.drawable.captain, 2016));
            add(new Movies("Dragon ball super", R.drawable.dbs, 2016));
            add(new Movies("Hulk", R.drawable.hulk, 2016));
            add(new Movies("Iron Man", R.drawable.ironman, 2016));
            add(new Movies("Naruto", R.drawable.kurama, 2016));
            add(new Movies("La liga de la justicia", R.drawable.dc, 2016));
            add(new Movies("Escuadron suicida", R.drawable.suicide, 2016));
            add(new Movies("Civil War", R.drawable.tanos, 2016));
            add(new Movies("Wolverine", R.drawable.wolverine, 2016));
        }};
    }

    private void recyclerLoad() {
        //mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setAdapter(mAdapter);
    }
}