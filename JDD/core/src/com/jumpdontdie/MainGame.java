package com.jumpdontdie;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class MainGame extends Game {

    @Override
    public void create() {
        setScreen(new MainGameScreen(this));
    }

}
