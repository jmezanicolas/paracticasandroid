package com.jumpdontdie.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by jmeza on 29/03/2017.
 */

public class ActorJugador extends Actor {
    private Texture jugardor;


    private boolean alive;

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public ActorJugador(Texture jugador) {
        this.jugardor = jugador;
        setSize(jugador.getWidth(), jugador.getHeight());
        this.alive = true;
    }

    @Override
    public void act(float delta) {

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(jugardor, getX(), getY());
    }
}
