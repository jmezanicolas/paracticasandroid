package com.jumpdontdie.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by jmeza on 29/03/2017.
 */

public class ActorSpike extends Actor {
    private TextureRegion spike;

    public ActorSpike(TextureRegion spike) {
        this.spike = spike;
        setSize(spike.getRegionWidth(), spike.getRegionHeight());
    }

    @Override
    public void act(float delta) {
        float x = getX();
        setX(getX() - 250 * delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(spike, getX(), getY());
    }
}
