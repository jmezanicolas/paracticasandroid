package com.jumpdontdie;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.jumpdontdie.actors.ActorJugador;
import com.jumpdontdie.actors.ActorSpike;

/**
 * Created by jmeza on 29/03/2017.
 */

public class MainGameScreen extends BaseScreen {

    private Stage stage;
    private ActorJugador jugador;
    private ActorSpike spike;
    private Texture texturaJudador, texturaSpike;
    private TextureRegion regionSpike;

    public MainGameScreen(MainGame game) {
        super(game);
        texturaJudador = new Texture("player1.png");
        texturaSpike = new Texture("spike.png");
        regionSpike = new TextureRegion(texturaSpike, 0, 64, 128, 64);
    }

    @Override
    public void show() {
        stage = new Stage();

        stage.setDebugAll(true);


        jugador = new ActorJugador(texturaJudador);
        spike = new ActorSpike(regionSpike);
        stage.addActor(this.jugador);
        stage.addActor(this.spike);

        jugador.setPosition(20, 100);
        spike.setPosition(1500, 100);
    }

    @Override
    public void hide() {
        stage.dispose();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.4f, 0.5f, 0.8f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        comprobarColosiones();
        stage.draw();
    }

    private void comprobarColosiones() {
        if (jugador.isAlive() &&
                (jugador.getX() + jugador.getWidth() > spike.getX())) {
            jugador.setAlive(false);
        }
    }


}
