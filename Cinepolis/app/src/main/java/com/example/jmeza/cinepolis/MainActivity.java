package com.example.jmeza.cinepolis;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //loadJson();
    }

    private void loadJson() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.cinepolis.com.mx/Consumo.svc")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        CiudadesService cs = retrofit.create(CiudadesService.class);
        Call<List<Ciudades>> call = cs.getData("1,2,3,4,5,6,9,15");
        call.enqueue(new Callback<List<Ciudades>>() {
            @Override
            public void onResponse(Call<List<Ciudades>> call, Response<List<Ciudades>> response) {
                String a = "";
            }

            @Override
            public void onFailure(Call<List<Ciudades>> call, Throwable t) {
                String a = "";
            }
        });
    }
}
