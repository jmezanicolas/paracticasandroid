package com.example.jmeza.cinepolis;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by jmeza on 06/03/2017.
 */

public interface CiudadesService {


    @GET("/json/ObtenerCiudadesPaises/{idsPaises}")
    Call<List<Ciudades>> getData(@Path("idsPaises") String idsPaises);
}
