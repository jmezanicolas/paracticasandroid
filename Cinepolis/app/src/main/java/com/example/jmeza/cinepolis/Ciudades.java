package com.example.jmeza.cinepolis;

/**
 * Created by jmeza on 06/03/2017.
 */

public class Ciudades {
    private String estado;
    private String nombre;
    private String pais;
    private String uris;
    private int id;
    private int idEstado;
    private int idPais;
    private Double latitud;
    private Double longitud;

    public Ciudades() {
    }

    public Ciudades(String estado, String nombre, String pais, String uris, int id, int idEstado, int idPais, Double latitud, Double longitud) {
        this.estado = estado;
        this.nombre = nombre;
        this.pais = pais;
        this.uris = uris;
        this.id = id;
        this.idEstado = idEstado;
        this.idPais = idPais;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getUris() {
        return uris;
    }

    public void setUris(String uris) {
        this.uris = uris;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

}
