package com.example.jmeza.mylogin.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by jmeza on 03/03/2017.
 */

public class Util {

    public static String getUserMailPrefs(SharedPreferences prefs) {
        return prefs.getString("email", "");
    }

    public static String getUserPasswordPrefs(SharedPreferences prefs) {
        return prefs.getString("pass", "");
    }

    public static void removeSharedPreferences(SharedPreferences prefs) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("email");
        editor.remove("pass");
        editor.apply();
    }
}
