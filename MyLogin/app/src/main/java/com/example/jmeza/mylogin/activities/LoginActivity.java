package com.example.jmeza.mylogin.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.jmeza.mylogin.R;
import com.example.jmeza.mylogin.utils.Util;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private SharedPreferences prefs;
    private EditText email;
    private EditText password;
    private Switch remember;
    private Button logIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        bindUI();
        setCredentialsIfExist();

        logIn.setOnClickListener(this);
    }

    private void setCredentialsIfExist() {
        String _email = Util.getUserMailPrefs(prefs);
        String _password = Util.getUserPasswordPrefs(prefs);
        if (!TextUtils.isEmpty(_email) && !TextUtils.isEmpty(_password)) {
            email.setText(_email);
            password.setText(_password);
        }
    }


    @Override
    public void onClick(View v) {
        String strEmail = email.getText().toString();
        String strPassword = password.getText().toString();
        if (login(strEmail, strPassword)) {
            goToMain();
            saveOnPreferences(strEmail, strPassword);
        }
    }

    private void bindUI() {
        email = (EditText) findViewById(R.id.editTextEmail);
        password = (EditText) findViewById(R.id.editTextPass);
        remember = (Switch) findViewById(R.id.switchRemember);
        logIn = (Button) findViewById(R.id.buttonLogin);
    }

    private boolean login(String email, String password) {
        if (!isValidEmail(email)) {
            Toast.makeText(this, "Email is not valid, please try again", Toast.LENGTH_LONG).show();
            return false;
        } else if (!isValidPassword(password)) {
            Toast.makeText(this, "Password is not valid, please try again", Toast.LENGTH_LONG).show();
            return false;
        } else {
            Toast.makeText(this, "Correct", Toast.LENGTH_LONG).show();
            return true;
        }

    }

    private void saveOnPreferences(String mail, String password) {
        if (remember.isChecked()) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("email", mail);
            editor.putString("pass", password);
            editor.commit();
            editor.apply();
        }
    }

    private boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValidPassword(String password) {
        return password.length() >= 4;
    }

    private void goToMain() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
