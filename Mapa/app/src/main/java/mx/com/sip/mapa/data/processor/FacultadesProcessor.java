package mx.com.sip.mapa.data.processor;

import mx.com.sip.mapa.MyApplication;
import mx.com.sip.mapa.data.bases.BusProvider;
import mx.com.sip.mapa.data.bases.EventProcessor;
import mx.com.sip.mapa.data.events.GetFacultadesEvent;
import mx.com.sip.mapa.data.events.SendFacultadesEvent;
import mx.com.sip.mapa.data.models.FacultadesResponse;
import mx.com.sip.mapa.data.service.FacultadesServiceImp;

/**
 * Created by Packp-pc on 01/05/2017.
 */

public class FacultadesProcessor implements EventProcessor<GetFacultadesEvent>, FacultadesServiceImp.FacultadesListener {
    @Override
    public void process(MyApplication aplicacion, GetFacultadesEvent event) {
        FacultadesServiceImp service = new FacultadesServiceImp(aplicacion, this);
        service.success();
    }

    @Override
    public boolean canProcess(Object event) {
        return event instanceof GetFacultadesEvent;
    }

    @Override
    public void onSuccess(FacultadesResponse resp) {
        BusProvider.getInstance().post(new SendFacultadesEvent(true, null, resp));
    }

    @Override
    public void onFailure(String error) {
        BusProvider.getInstance().post(new SendFacultadesEvent(false, error, null));
    }
}
