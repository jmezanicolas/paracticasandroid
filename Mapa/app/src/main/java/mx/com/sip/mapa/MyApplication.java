package mx.com.sip.mapa;

import android.app.Application;

import mx.com.sip.mapa.data.bases.BusProvider;


/**
 * Created by jmeza on 28/03/2017.
 */

public class MyApplication extends Application {
    private AppManager mManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mManager = new AppManager(this);
        BusProvider.getInstance().register(mManager);
    }

}
