package mx.com.sip.mapa.ui.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import mx.com.sip.mapa.R;
import mx.com.sip.mapa.data.models.FacultadesResponse;
import mx.com.sip.mapa.ui.base.BaseFragment;
import mx.com.sip.mapa.ui.presenter.FacultadesPresenter;
import mx.com.sip.mapa.ui.views.FacultadesView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FacultadesFragment extends BaseFragment implements FacultadesView {
    private ProgressDialog mProgress;
    private List<FacultadesResponse> facultades = new ArrayList<>();
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private FacultadesPresenter mPresenter;

    public static FacultadesFragment newInstance() {
        return new FacultadesFragment();
    }

    public FacultadesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_facultades, container, false);
        mPresenter = new FacultadesPresenter(this);
        mLayoutManager = new LinearLayoutManager(getContext());
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getFacultades();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.onPause();

    }

    @Override
    public void showProgressDialog() {
        mProgress = ProgressDialog.show(getContext(), null, "Cargando...", true);
    }

    @Override
    public void hideProgressDialog() {
        if (mProgress != null) {
            mProgress.dismiss();
        }
    }

    @Override
    public void onLoadError(String error) {
        if (getView() != null) {
            snackbar(getView(), error).show();
        }
    }

    @Override
    public void onShowBody(FacultadesResponse usersResponse) {
        String a = "";
//        facultades = usersResponse.getData();
    }


}
