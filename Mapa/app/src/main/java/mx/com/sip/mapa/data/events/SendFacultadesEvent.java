package mx.com.sip.mapa.data.events;

import mx.com.sip.mapa.data.bases.BaseEvent;
import mx.com.sip.mapa.data.models.FacultadesResponse;
import mx.com.sip.mapa.data.models.UniversidadesResponse;

/**
 * Created by Packp-pc on 01/05/2017.
 */

public class SendFacultadesEvent extends BaseEvent{
    private boolean isSuccess;
    private String message;
    private FacultadesResponse response;

    public SendFacultadesEvent(boolean isSuccess, String message, FacultadesResponse response) {
        this.isSuccess = isSuccess;
        this.message = message;
        this.response = response;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public FacultadesResponse getResponse() {
        return response;
    }
}
