package mx.com.sip.mapa.data.bases;


import mx.com.sip.mapa.MyApplication;

/**
 * Created by jmeza on 28/03/2017.
 */

public interface EventProcessor<T> {

    void process(MyApplication aplicacion, T event);

    boolean canProcess(Object event);
}
