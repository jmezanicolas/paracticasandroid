package mx.com.sip.mapa.data.processor;

import mx.com.sip.mapa.MyApplication;
import mx.com.sip.mapa.data.bases.BusProvider;
import mx.com.sip.mapa.data.bases.EventProcessor;
import mx.com.sip.mapa.data.events.GetUniversidadesEvent;
import mx.com.sip.mapa.data.events.SendUniversidadesEvent;
import mx.com.sip.mapa.data.models.UniversidadesResponse;
import mx.com.sip.mapa.data.service.UniversidadesServiceImp;

/**
 * Created by Packp-pc on 01/05/2017.
 */

public class UniversidadesProcessor implements EventProcessor<GetUniversidadesEvent>, UniversidadesServiceImp.UniversidadesListener{
    @Override
    public void process(MyApplication aplicacion, GetUniversidadesEvent event) {
        UniversidadesServiceImp service = new UniversidadesServiceImp(aplicacion, this);
        service.success();
    }

    @Override
    public boolean canProcess(Object event) {
        return event instanceof GetUniversidadesEvent;
    }

    @Override
    public void onSuccess(UniversidadesResponse resp) {
        BusProvider.getInstance().post(new SendUniversidadesEvent(true, null, resp));
    }

    @Override
    public void onFailure(String error) {
        BusProvider.getInstance().post(new SendUniversidadesEvent(false, error, null));
    }
}
