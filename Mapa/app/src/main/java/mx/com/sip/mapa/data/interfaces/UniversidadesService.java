package mx.com.sip.mapa.data.interfaces;

import mx.com.sip.mapa.data.models.UniversidadesResponse;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Packp-pc on 01/05/2017.
 */

public interface UniversidadesService {
    @GET("/repository-server/universidades")
    void getUniversidades(Callback<UniversidadesResponse> callback);
}
