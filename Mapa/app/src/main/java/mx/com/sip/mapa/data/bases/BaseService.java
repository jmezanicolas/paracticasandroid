package mx.com.sip.mapa.data.bases;

import android.content.Context;

/**
 * Created by jmeza on 28/03/2017.
 */

public class BaseService {


    protected Context mContext;
    protected String mEndpoint;


    public BaseService(Context context) {
        mContext = context;
        mEndpoint = "http://192.168.62.50:8080";

    }

    public BaseService() {
    }


    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public String getmEndpoint() {
        return mEndpoint;
    }

    public void setmEndpoint(String mEndpoint) {
        this.mEndpoint = mEndpoint;
    }


}
