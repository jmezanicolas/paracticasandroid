package mx.com.sip.mapa.data.service;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import mx.com.sip.mapa.R;
import mx.com.sip.mapa.data.bases.BaseEvent;
import mx.com.sip.mapa.data.bases.BaseService;
import mx.com.sip.mapa.data.interfaces.FacultadesService;
import mx.com.sip.mapa.data.interfaces.UniversidadesService;
import mx.com.sip.mapa.data.models.FacultadesResponse;
import mx.com.sip.mapa.data.models.UniversidadesResponse;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Packp-pc on 01/05/2017.
 */

public class FacultadesServiceImp extends BaseService {
    private FacultadesListener listener;
    private FacultadesService service;

    public FacultadesServiceImp(Context context, FacultadesListener facuListener) {
        super(context);
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setConnectTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setWriteTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        mContext = context;
        listener = facuListener;
        service = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(mEndpoint)
                .build()
                .create(FacultadesService.class);
        String a = "";
    }

    public void success() {

        service.getUniversidades(new retrofit.Callback<FacultadesResponse>() {
            @Override
            public void success(FacultadesResponse uniResponses, Response response) {
                String a ="";
                if (!uniResponses.getData().isEmpty()) {
                    if (listener != null)
                        listener.onSuccess(uniResponses);
                } else {
                    if (listener != null) {
                        listener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (listener != null) {
                    listener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                }
            }
        });
    }

    public interface FacultadesListener {
        void onSuccess(FacultadesResponse resp);

        void onFailure(String error);

    }
}
