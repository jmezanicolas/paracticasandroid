package mx.com.sip.mapa.data.models;

/**
 * Created by Packp-pc on 06/05/2017.
 */

class Pagination {

    private Paging paging;

    public Paging getPaging ()
    {
        return paging;
    }

    public void setPaging (Paging paging)
    {
        this.paging = paging;
    }
    public class Paging
    {
        private String max;

        private String previous;

        private String next;

        public String getMax ()
        {
            return max;
        }

        public void setMax (String max)
        {
            this.max = max;
        }

        public String getPrevious ()
        {
            return previous;
        }

        public void setPrevious (String previous)
        {
            this.previous = previous;
        }

        public String getNext ()
        {
            return next;
        }

        public void setNext (String next)
        {
            this.next = next;
        }

    }
}
