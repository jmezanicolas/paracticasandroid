package mx.com.sip.mapa.data.service;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

import mx.com.sip.mapa.R;
import mx.com.sip.mapa.data.bases.BaseService;
import mx.com.sip.mapa.data.interfaces.UniversidadesService;
import mx.com.sip.mapa.data.models.UniversidadesResponse;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

/**
 * Created by Packp-pc on 01/05/2017.
 */

public class UniversidadesServiceImp extends BaseService {
    private UniversidadesListener listener;
    private UniversidadesService service;
    public UniversidadesServiceImp(Context context, UniversidadesListener uniListener){
        super(context);
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setConnectTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setWriteTimeout(640 * 1000, TimeUnit.MILLISECONDS);
        mContext = context;
        listener = uniListener;
        service = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(mEndpoint)
                .build()
                .create(UniversidadesService.class);
    }

    public void success() {

        service.getUniversidades(new retrofit.Callback<UniversidadesResponse>() {
            @Override
            public void success(UniversidadesResponse uniResponses, Response response) {
                if (!uniResponses.getData().isEmpty()) {
                    if (listener != null)
                        listener.onSuccess(uniResponses);
                } else {
                    if (listener != null) {
                        listener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                    }
                }
            }
            @Override
            public void failure(RetrofitError error) {
                if (listener != null) {
                    listener.onFailure(mContext.getString(R.string.actualmente_el_servicio_no_esta_disponible));
                }
            }
        });
    }

    public interface UniversidadesListener {
        void onSuccess(UniversidadesResponse resp);

        void onFailure(String error);

    }

}
