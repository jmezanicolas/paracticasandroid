package mx.com.sip.mapa;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import mx.com.sip.mapa.data.bases.BaseEvent;
import mx.com.sip.mapa.data.bases.EventProcessor;
import mx.com.sip.mapa.data.processor.FacultadesProcessor;
import mx.com.sip.mapa.data.processor.UniversidadesProcessor;

/**
 * Created by jmeza on 28/03/2017.
 */

public class AppManager {
    private MyApplication mApplication;
    private ArrayList<EventProcessor> mProcessor = new ArrayList<>();

    public AppManager(MyApplication application) {
        this.mApplication = application;
        initProcess();
    }

    private void initProcess() {
        mProcessor.add(new FacultadesProcessor());
        mProcessor.add(new UniversidadesProcessor());
    }

    @Subscribe
    public void onEvent(BaseEvent event) {
        EventProcessor processor = getProcessor(event);
        if (processor != null) {
            processor.process(mApplication, event);
        }
    }


    private EventProcessor getProcessor(BaseEvent event) {
        for (EventProcessor processor : mProcessor) {
            if (processor.canProcess(event)) {
                return processor;
            }
        }
        return null;
    }


}
